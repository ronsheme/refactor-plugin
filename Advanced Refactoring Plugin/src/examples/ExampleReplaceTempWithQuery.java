package examples;

public class ExampleReplaceTempWithQuery {
	private static int[] variables;
	private static boolean[] isVariableInitialized;

	public double foo(double quantity, double itemPrice){
	double basePrice = quantity * itemPrice;//this has some meaning
	if (basePrice > 1000)
	    return basePrice * 0.95;
	else
	    return basePrice * 0.98;
	}
	
	public static void setVariableValue(char c, int n) {
		int location = (int) c - 96;
		variables[location] = n;
		isVariableInitialized[location] = true;
	}

	public static int getVariableValue(char c) {
		int location = (int) c - 96;//this is duplicated
		if (!isVariableInitialized[location])
			return -1;
		return variables[location];
	}
}
