package examples;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.eclipse.jdt.core.dom.Statement;

public class ExampleConsolidateConditional {
	boolean someFlag;
	int getMax(int a, int b, int c)
	{		
		int max = a;
		if(a++>b){
			c=a+b;
			max++;
		}
		else{
			a++;
			c=a+b;
		}
		if(a < b){
			if (b<c){
				max = c;
				return max;
			}
			else{
				max = b;
				return max;
			}	
		}
		else{
			if (a<c){
				max = c;
				return max;
			}
			else
				return max;
		}
	}
	
	void readSomething(File f,int numOfBytes) throws IOException
	{
		FileInputStream ifs = new FileInputStream(f);
		byte[] arr;
		if(someFlag){
			someFlag = false;
			arr = new byte[numOfBytes];
			ifs.read(arr);
			numOfBytes = 1;
			ifs.close();
		}
		else{
			someFlag = true;
			numOfBytes /= 2;
			arr = new byte[numOfBytes];//cannot be pulled up
			ifs.read(arr);
			ifs.read(arr);
			numOfBytes = 0;
			ifs.close();
		}
	}
}
