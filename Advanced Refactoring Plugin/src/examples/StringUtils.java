package examples;

import java.util.Arrays;

public class StringUtils {

	public static String reverseString (String source){
		String reverse=new String();
		for(int i=1;i<=source.length();i++)
			reverse+=source.charAt(source.length()-i);
		return reverse;
	}
	
	public static String sortStringWords (String str){
		String[] splited=str.split(" ");
		String sorted="";
		Arrays.sort(splited);
		for(int i=0;i<splited.length;i++){
			sorted+=splited[i];
			if(i<splited.length-1)
				sorted+=" ";
		}
		return sorted;
	}
	
	public static String[] cutNLow(String[] str,int num){
		String[] cut=new String[str.length];
		for(int i=0;i<str.length;i++){
			if(str[i].length()>num)
				cut[i]=str[i].substring(0,num).toLowerCase();
			else
				cut[i]=str[i].toLowerCase();
		}
		return cut;
	}
	
	public static boolean isStringArraySorted(String[] strs, int n){
		String[] array=cutNLow(strs,n);
		String[] arrayS=cutNLow(strs,n);
		Arrays.sort(arrayS);
		if(Arrays.equals(array,arrayS))
			return true;
		return false;
	}
	
	public static String[] convertStringsToAcronyms(String[] input) {
		String[] acro=new String[input.length];
		for(int i=0;i<input.length;i++){
			if(input[i]==null)
				acro[i]=null;
			else
				acro[i]=input[i].replaceAll("[^A-Z]","");
		}
		return acro;
	}
	
	public static int[] stringHistogram(String a){
		int[] ab=new int[26];
		for(int i=0;i<26;i++)
			ab[i]=0;
		for(int i=0;i<a.length();i++)
			ab[(int)a.charAt(i)-(int)'a']++;
		return ab;
	}
	
	public static boolean areAnagrams(String a, String b){
		String newA=a.replaceAll(" ","").toLowerCase();
		String newB=b.replaceAll(" ","").toLowerCase();
		boolean anagram=Arrays.equals(stringHistogram(newA),stringHistogram(newB));
		return anagram;
	}
	
	public static void main(String[] args){
		
	}

}
