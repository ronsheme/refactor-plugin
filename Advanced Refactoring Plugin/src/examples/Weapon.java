package examples;

public class Weapon {
	
	private String name;
	private int firePower;
	private int maint;
	
	public Weapon(String name, int firePower, int annualMaintenanceCost) {
		this.name=name;
		this.firePower=firePower;
		this.maint=annualMaintenanceCost;
	}
	
	public String getName(){
		return this.name;
	}
	
	public int getFirePower(){
		return this.firePower;
	}
	
	public int getMaint(){
		return this.maint;
	}
	
	public String toString(){
		return ("Weapon [name="+getName()+
				", firePower="+getFirePower()+
				", annualMaintenanceCost="+getMaint()+"]");
	}
}
