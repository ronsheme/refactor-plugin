package examples;

public class ArrayUtils {
	
	public static int[] shiftArrayToTheRight(int[] array){
		int[] newArr=new int[array.length];
		for(int i=array.length-1;i>=0;i--){
			newArr[(i+1)%array.length]=array[i];
		}
		return newArr;
	}
	
	public static int sumArray(int[] array){
		int sum=0;
		for(int i=0;i<array.length;i++)
			sum+=array[i];
		return sum;
	}
	
	public static String[] fillStringArray(String[] strs, int[] repeats){
		
		String[] newArr=new String[sumArray(repeats)];
		int index=0;
		for(int i=0;i<strs.length;i++)
			for(int j=0;j<repeats[i];j++){
				newArr[index]=strs[i];
				index++;
			}
		return newArr;
	}
	
	public static int[][] transposeMatrix(int[][] matrix){
		int n=matrix.length;
		int m=matrix[0].length;
		int[][] newMat=new int[m][n];
		for(int i=0;i<n;i++)
			for(int j=0;j<m;j++)
				newMat[j][i]=matrix[i][j];
		return newMat;
	}
	
	public static int[] split(int[] input, int number){
		int index=0;
		int[] split=new int[input.length];
		for(int i=0;i<input.length;i++)
			if(input[i]==number)
				index++;
			else
				split[index]++;
		return split;
	}
	
	public static int strikes(int[] split){
		int counter=0;
		for(int i=0;i<split.length;i++)
			if(split[i]!=0)
				counter++;
		return counter;
	}
	
	private static int highestSq(int[] split) {
		for(int i=split.length-1;i>=0;i--)
			if(split[i]!=0)
				return i;
		return 0;
	}
	
	public static int[][] splitArrayByNum(int[] input, int number){
		int[] split=split(input,number);
		int counter=strikes(split);
		int highest=highestSq(split);
		if(counter==0)
			return new int[0][0];
		int index=0;
		int offset=0;
		int[][] arr=new int[counter][];
		for(int i=0;i<=highest;i++){
			if(split[i]!=0){
				arr[i-offset]=new int[split[i]];
				int a=0;
				for(int j=index;j<index+split[i];j++){
					arr[i-offset][a]=input[j];
					a++;
				}
				index+=split[i]+1;
			}
			else{
				index++;
				offset++;
			}
		}
		return arr;
	}
	
	public static void main(String[] args){
		
	}
}
