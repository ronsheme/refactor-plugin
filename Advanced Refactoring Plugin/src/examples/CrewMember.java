package examples;

public interface CrewMember {
	
	public int getAge();
	
	public String getName();
	
	public int getYearsInService();
}
