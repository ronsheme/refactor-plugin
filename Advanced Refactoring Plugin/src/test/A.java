package test;

public class A {

	protected int a;
	public int b;
	protected int c;
	private int x;
	private int y;
	public int noUse;

	public A(int a, int b, int c) {
		this.a = a;
		this.b = b;
		this.c = c;
	}

	public void hello() {
		System.out.println("Hello");
		x = 1;
		y = 2;
	}
	
	private void number(int i) {
		increase(this.x);
		System.out.println(x + "is now bigger by ten!");
	}
	
	public static int increase(int i) {
		i = i + 10;
		return i;
	}
}