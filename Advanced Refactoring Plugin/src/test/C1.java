package test;

public class C1 extends A1 {

	protected int x;
	
	public C1(int a, int b, int c) {
		super(a, b, c);
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}
	

}