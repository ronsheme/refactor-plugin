package test;

import java.util.ArrayList;
import java.util.List;
import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.FieldDeclaration;
import org.eclipse.jdt.core.dom.IVariableBinding;
import org.eclipse.jdt.core.dom.Statement;
import org.eclipse.jdt.core.dom.TypeDeclaration;
import org.eclipse.jdt.core.dom.VariableDeclarationFragment;

/**
 * 
 * @author Ron Shemer FieldListerVisitor lists all the fields in a class
 *
 */
public class FieldListerVisitor extends ASTVisitor {

	List<VariableDeclarationFragment> FieldDeclaration = new ArrayList<VariableDeclarationFragment>();
	boolean inFirstDecl = true;
	
	public List<VariableDeclarationFragment> getFieldList() {
		return FieldDeclaration;
	}

	@Override
	public boolean visit(FieldDeclaration node) {
		for (Object o : node.fragments())
			FieldDeclaration.add((VariableDeclarationFragment)o);
		return false;
	};

	@Override
	public boolean visit(TypeDeclaration node) {
		if(inFirstDecl)
		{
			inFirstDecl = false;
			return super.visit(node);
		}
		return false;
	}
	// TODO Override other methods and prevent AST traversal in order to improve
	// time complexity
}