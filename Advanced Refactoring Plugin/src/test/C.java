package test;

public class C extends A {

	private int x;
	
	public C(int a, int b, int c) {
		super(a, b, c);
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}
	
	

}
