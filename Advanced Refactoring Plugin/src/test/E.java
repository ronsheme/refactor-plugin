package test;

public class E extends C {

	public E(int a, int b, int c) {
		super(a, b, c);
	}

	private void foo(int a, int b, int c) {
		this.a = a;
		this.b = b;
		this.c = c;
	}
}
