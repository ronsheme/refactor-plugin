package utils;

import java.util.LinkedList;
import java.util.List;

public class inheritanceUtils {

	public static List<Class<?>> inheritsFromClass(Class<?> c,String srcFolderString) throws ClassNotFoundException {
		List<Class<?>> result = new LinkedList<Class<?>>();
		List<String> classes = JavaProjectFileCollector.getJavaFiles(srcFolderString);

		for (String string : classes) {
			Class<?> currclass = Class.forName(string);
			if (currclass.getSuperclass() != null && currclass.getSuperclass().getSimpleName().equals(c.getSimpleName())) {
				result.add(currclass);
			}
		}
		return result;
	}
}
