package utils.visitors;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jdt.core.dom.AbstractTypeDeclaration;
import org.eclipse.jdt.core.dom.ArrayAccess;
import org.eclipse.jdt.core.dom.ArrayInitializer;
import org.eclipse.jdt.core.dom.BooleanLiteral;
import org.eclipse.jdt.core.dom.CastExpression;
import org.eclipse.jdt.core.dom.CharacterLiteral;
import org.eclipse.jdt.core.dom.ConditionalExpression;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.FieldAccess;
import org.eclipse.jdt.core.dom.IVariableBinding;
import org.eclipse.jdt.core.dom.IfStatement;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.MethodInvocation;
import org.eclipse.jdt.core.dom.NullLiteral;
import org.eclipse.jdt.core.dom.NumberLiteral;
import org.eclipse.jdt.core.dom.SimpleName;
import org.eclipse.jdt.core.dom.Statement;
import org.eclipse.jdt.core.dom.StringLiteral;
import org.eclipse.jdt.core.dom.ThisExpression;

import utils.ASTMisc;
import utils.VarIndex;
import utils.visitors.TempAssignmentCounterVisitor;

/**
 * @author Ron Shemer ReplaceTempWithQueryVisitor Counts the number of
 *         assignments to a local variable (declared in a method). If the
 *         variable is assigned more than once it is ignored -
 *         SplotTemporaryVariableVisitor handles this case. If the variable is
 *         assigned once and this assignment is not a query (MethodInvocation)
 *         it is suggested to be assigned through a query.
 *
 */
public class ReplaceTempWithQueryVisitor extends TempAssignmentCounterVisitor {

	@Override
	public boolean visit(MethodDeclaration node) {
		setNames(node);
		if (currentMethodBinding != null)
			methodToVariables.put(currentMethodBinding, new ArrayList<IVariableBinding>());
		else
			methodStringToVariables.put(currentMethodString, new ArrayList<>());

		node.getBody().accept(this);// should add all variable to this method's
									// list and count assignments
		List<IVariableBinding> vars = (currentMethodBinding == null) ? methodStringToVariables.get(currentMethodString)
				: methodToVariables.get(currentMethodBinding);
		for (IVariableBinding variable : vars) {
			if (variablesToAssignments.get(variable) == null || variable == null)
				continue;
			if (variablesToAssignments.get(variable).size() == 1) {
				Expression e = variablesToAssignments.get(variable).get(0);
				if (!allowedExpressions(e, variable)) {
					DetectSideEffectVisitor DSEVisitor = new DetectSideEffectVisitor();
					variablesToAssignments.get(variable).get(0).accept(DSEVisitor);
					if (!DSEVisitor.isFoundSideEffect()) {
						tempStringsToRefactor.add(variable.getName() + " in method " + currentMethodString + "\n");
						Statement s = ASTMisc.getStatementParent(variablesToAssignments.get(variable).get(0));
						tempPositionsToRefactor.add(new VarIndex(s.getStartPosition(), s.getLength(),variable.getName()));
					}
				}
			}
		}

		return false;
	}

	private boolean allowedExpressions(Expression e, IVariableBinding variable) {
		if ((e instanceof MethodInvocation) || (e instanceof ArrayAccess) || (e instanceof SimpleName)
				|| (e instanceof BooleanLiteral) || (e instanceof NullLiteral) || (e instanceof StringLiteral)
				|| (e instanceof NumberLiteral) || (e instanceof ConditionalExpression)
				|| (e instanceof CharacterLiteral)
				|| (e instanceof ArrayInitializer)
				|| (e instanceof FieldAccess))
			return true;
		if (e instanceof CastExpression && allowedExpressions(((CastExpression) e).getExpression(), variable))
			return true;
		/*if (e.getParent() instanceof IfStatement
				&& countConditionalAssignment((IfStatement) e.getParent(), variable) != 1)
			return true;*/
		return false;
	}
}
