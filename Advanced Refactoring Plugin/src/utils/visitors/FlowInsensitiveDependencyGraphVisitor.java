package utils.visitors;

import java.util.List;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.Assignment;
import org.eclipse.jdt.core.dom.Assignment.Operator;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.IBinding;
import org.eclipse.jdt.core.dom.IVariableBinding;
import org.eclipse.jdt.core.dom.MethodInvocation;
import org.eclipse.jdt.core.dom.PostfixExpression;
import org.eclipse.jdt.core.dom.PrefixExpression;
import org.eclipse.jdt.core.dom.SimpleName;
import org.eclipse.jdt.core.dom.VariableDeclarationFragment;
import org.jgrapht.Graph;
import org.jgrapht.graph.DefaultEdge;

import utils.GraphsUtils;

/**
 * 
 * @author Ron Shemer
 * 
 *         Variable x is dependent on variable y if in an assignment x is in the
 *         LHS and y is on the RHS, or in method invocation where y is a
 *         parameter to a method invoked in x
 *
 */

public abstract class FlowInsensitiveDependencyGraphVisitor extends ASTVisitor {

	protected Graph<IVariableBinding, DefaultEdge> g;
	protected IVariableBinding currLHS;
	protected List<IVariableBinding> fieldList;
	protected int a;

	public List<IVariableBinding> getFieldList() {
		return fieldList;
	}

	public Graph<IVariableBinding, DefaultEdge> getDependencyGraph() {
		return g;
	}

	@Override
	public boolean visit(CompilationUnit node) {
		FieldListerVisitor FLVisitor = new FieldListerVisitor();
		node.accept(FLVisitor);
		fieldList = FLVisitor.getFieldBindings();
		return super.visit(node);

	}

	@Override
	public boolean visit(SimpleName node) {
		if (!(node.resolveBinding() instanceof IVariableBinding) || currLHS == null)
			return false;
		IVariableBinding currVar = (IVariableBinding) node.resolveBinding();
		GraphsUtils.safeAddEdge(g,currLHS,currVar);
		return false;

	}

	public boolean visit(Assignment node) {
		if (!setLHSBinding(node))
			return false;
		if (node.getOperator() != Operator.ASSIGN)
			GraphsUtils.safeAddEdge(g,currLHS,currLHS);

		if (node.getRightHandSide() != null)
			node.getRightHandSide().accept(this);
		currLHS = null;
		return false;
	}

	@Override
	public boolean visit(PrefixExpression node) {
		if ((node.getOperator() == PrefixExpression.Operator.DECREMENT
				|| node.getOperator() == PrefixExpression.Operator.INCREMENT)
				&& node.getOperand() instanceof SimpleName) {
			if (!setLHSBinding((SimpleName) node.getOperand()))
				return false;
			GraphsUtils.safeAddEdge(g,currLHS,currLHS);
		}
		return false;
	}

	@Override
	public boolean visit(PostfixExpression node) {
		if (!(node.getOperand() instanceof SimpleName))
			return false;
		if (!setLHSBinding((SimpleName) node.getOperand()))
			return false;
		GraphsUtils.safeAddEdge(g,currLHS,currLHS);
		return false;
	}

	public boolean visit(VariableDeclarationFragment node) {
		if (!setLHSBinding(node))
			return false;
		if (node.getInitializer() != null)
			node.getInitializer().accept(this);
		currLHS = null;
		return false;
	}
	@Override
	public boolean visit(MethodInvocation node) {
		boolean currLSHSetHere;
		if (currLHS != null) {
			if (node.getExpression() != null)
				node.getExpression().accept(this);
			currLSHSetHere = false;
		} else if (!setLHSBinding(node))
			return false;
		else
			currLSHSetHere = true;
		if (node.arguments() != null)
			for (Object o : node.arguments())
				((ASTNode) o).accept(this);
		if (currLSHSetHere)
			currLHS = null;
		return false;
	}

	protected boolean setLHSBinding(MethodInvocation node) {
		if (node.getExpression() == null)
			return false;
		// check that is simple variable
		if (!(node.getExpression() instanceof SimpleName)) {
			if (node.getExpression() instanceof MethodInvocation)
				return setLHSBinding((MethodInvocation) node.getExpression());
			else
				return false;
		}
		IBinding tempBiding = ((SimpleName) node.getExpression()).resolveBinding();
		if (!(tempBiding instanceof IVariableBinding))
			return false;
		currLHS = (IVariableBinding) tempBiding;
		return true;
	}

	protected boolean setLHSBinding(VariableDeclarationFragment node) {
		if (node.getInitializer() == null)
			return false;
		// check that is simple variable
		if (!(node.getName() instanceof SimpleName))
			return false;
		IBinding tempBiding = ((SimpleName) node.getName()).resolveBinding();
		if (!(tempBiding instanceof IVariableBinding))
			return false;
		currLHS = (IVariableBinding) tempBiding;
		return true;
	}

	protected boolean setLHSBinding(Assignment node) {
		// check that is simple variable
		if (!(node.getLeftHandSide() instanceof SimpleName))
			return false;
		IBinding tempBiding = ((SimpleName) node.getLeftHandSide()).resolveBinding();
		if (!(tempBiding instanceof IVariableBinding))
			return false;
		currLHS = (IVariableBinding) tempBiding;
		return true;
	}

	private boolean setLHSBinding(SimpleName node) {
		IBinding tempBiding = node.resolveBinding();
		if (!(tempBiding instanceof IVariableBinding))
			return false;
		currLHS = (IVariableBinding) tempBiding;
		return true;
	}
}