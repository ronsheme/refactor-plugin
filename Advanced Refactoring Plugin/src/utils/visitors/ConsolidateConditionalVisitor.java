package utils.visitors;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.jdt.core.dom.ASTMatcher;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.IVariableBinding;
import org.eclipse.jdt.core.dom.IfStatement;
import org.eclipse.jdt.core.dom.SimpleName;
import org.eclipse.jdt.core.dom.Statement;
import org.eclipse.jdt.core.dom.VariableDeclarationFragment;
import org.eclipse.jdt.core.dom.VariableDeclarationStatement;
import org.jgrapht.Graph;
import org.jgrapht.graph.DefaultEdge;

import utils.GraphsUtils;
import utils.IndexPair;

/**
 * 
 * ConsolidateConditionalVisitor traverses the AST until an IfStatement appears.
 * Then, if it owns an Else clause, it finds statements that are common for the
 * Then and Else clauses by scanning from top to bottom and then from bottom to
 * top. The direction of the scan implies whether the matching statements may be
 * moved before or after the conditional in the flow of the program.
 *
 */
public class ConsolidateConditionalVisitor extends ASTVisitor {

	List<IndexPair> pullUpStatementsLines = new ArrayList<IndexPair>();
	List<IndexPair> pushDownStatementsLines = new ArrayList<IndexPair>();
	List<Statement> taken = new ArrayList<Statement>();
	Set<Statement> interfered = new HashSet<Statement>();
	List<IVariableBinding> conditionalChangedVars;
	List<IVariableBinding> conditionalUsedVars;
	Statement ifStmt;

	public List<IndexPair> getPullUpStatementsLines() {
		return pullUpStatementsLines;
	}

	public List<IndexPair> getPushDownStatementsLines() {
		return pushDownStatementsLines;
	}

	/**
	 * visit(IfStatement node): Checks whether an ElseSatement exists, if so
	 * flatten the Then and Else statements for the forward and backward scan,
	 * keep a list of variables that are affected by the conditional itself (in
	 * order not to pull up statements that involve these variables) and call
	 * forward and backward scan
	 */
	@Override
	public boolean visit(IfStatement node) {
		Statement elseStatement = node.getElseStatement();
		if (elseStatement == null)
			return false;
		List<Statement> flatThenStatementList = ConsolidateConditionalUtils.getFlatStatement(node.getThenStatement());
		List<Statement> flatElseStatementList = ConsolidateConditionalUtils.getFlatStatement(elseStatement);
		ChangedVariableListerVisitor CVLVisitor = new ChangedVariableListerVisitor();
		node.getExpression().accept(CVLVisitor);
		this.conditionalChangedVars = CVLVisitor.getChangedVariables();
		this.conditionalUsedVars = getDepList(node.getExpression());
		this.ifStmt = node;

		matchForward(flatThenStatementList, flatElseStatementList);
		matchBackward(flatThenStatementList, flatElseStatementList);
		return true;// should work also for sub conditionals
	}

	/*
	 * 
	 * @param flatThenStatementList
	 * @param flatElseStatementList
	 * 
	 *            updates the pushDownStatementsLines with identical statements
	 *            from thenStatement and elseStatement, starting from the
	 *            bottom. Once no more statements match another method for
	 *            disordered matches is invoked.
	 */
	private void matchBackward(List<Statement> flatThenStatementList, List<Statement> flatElseStatementList) {
		// iterate backwards
		int thenBackwardIndex = flatThenStatementList.size() - 1;
		int elseBackwardIndex = flatElseStatementList.size() - 1;
		Statement currElseStmt, currThenStmt;
		while (elseBackwardIndex > -1 && thenBackwardIndex > -1) {
			currThenStmt = flatThenStatementList.get(thenBackwardIndex);
			currElseStmt = flatElseStatementList.get(elseBackwardIndex);
			if (taken.contains(currThenStmt) || taken.contains(currElseStmt)
					|| !ConsolidateConditionalUtils.statementsEqual(currElseStmt, currThenStmt)
					|| (currElseStmt instanceof VariableDeclarationStatement
							&& declarationContainsInitializer((VariableDeclarationStatement) currElseStmt))) {
				while (findBackwardDisorderedMatches(currElseStmt, currThenStmt, elseBackwardIndex, thenBackwardIndex,
						flatElseStatementList, flatThenStatementList)) {
					;
				}
				return;
			}
			elseBackwardIndex--;
			thenBackwardIndex--;
			if (currElseStmt instanceof VariableDeclarationStatement)
				continue;
			if (!currThenStmt.subtreeMatch(new BindingMatcher(), currElseStmt))
				return;
			addToList(pushDownStatementsLines, currElseStmt, currThenStmt);
		}
	}

	/*
	 * 
	 * @param currElseStmt
	 *            the first statement in Else clause that is not identical to
	 *            the matching one (by order) in the Then clause
	 * @param currThenStmt
	 *            the first statement in Then clause that is not identical to
	 *            the matching one (by order) in the Else clause
	 * @param elseBackwardIndex
	 *            index of currElseStmt in flatElseStatementList
	 * @param thenBackwardIndex
	 *            index of currElseStmt in flatThenStatementList
	 * @param flatElseStatementList
	 * @param flatThenStatementList
	 * @return boolean value that indicated whether the analysis should be
	 *         performed again, if any statement that interfered another is
	 *         being pushed down For the Then and Else clauses this creates a
	 *         list of statements that later statements in the clause do not
	 *         depend on them, and then they are being compared in order to find
	 *         identical statements
	 */
	private boolean findBackwardDisorderedMatches(Statement currElseStmt, Statement currThenStmt, int elseBackwardIndex,
			int thenBackwardIndex, List<Statement> flatElseStatementList, List<Statement> flatThenStatementList) {

		List<Statement> unchangedThenStmts = getBackwardStatementsWithUnchangedDependencies(currThenStmt,
				thenBackwardIndex, flatThenStatementList, false);
		List<Statement> unchangedElseStmts = getBackwardStatementsWithUnchangedDependencies(currElseStmt,
				elseBackwardIndex, flatElseStatementList, true);

		return compareStatementsWithUnchangedDependencies(unchangedElseStmts, unchangedThenStmts,
				pushDownStatementsLines);
	}

	/*
	 * 
	 * @param firstStmt
	 * @param backwardIndex
	 * @param flatStatementList
	 * @param elseStmt
	 * @return list of statements that can be pushed down
	 * 
	 *         For each statement s iterate all statements below (after) it in
	 *         the flatStatementList and add it to the unchanged list if it none
	 *         of the statements depends on a variable that might be changed in
	 *         s. For this it is necessary to construct an updated flow
	 *         sensitive dependency graph
	 */
	private List<Statement> getBackwardStatementsWithUnchangedDependencies(Statement firstStmt, int backwardIndex,
			List<Statement> flatStatementList, boolean elseStmt) {

		Graph<IVariableBinding, DefaultEdge> depGraph;
		LocalVariableFlowSensitiveDependencyGraphVisitor LVFSDGVisitor;
		List<IVariableBinding> dependencies;
		Map<Statement, List<IVariableBinding>> stmtsWithChangedVarListCache = new HashMap<>();
		List<Statement> ret = new ArrayList<>();
		boolean dependencyChanged = false;
		Statement tempStmt, stmt;
		int tempIterationIndex = 0, firstInedx = backwardIndex;

		ret.add(firstStmt);
		if (backwardIndex <= 0)
			return ret;
		addStmtChangedListToCache(firstStmt, stmtsWithChangedVarListCache);
		backwardIndex--;
		for (; backwardIndex >= 0; backwardIndex--) {
			stmt = flatStatementList.get(backwardIndex);
			addStmtChangedListToCache(stmt, stmtsWithChangedVarListCache);
			dependencies = getDepList(stmt);
			tempIterationIndex = firstInedx;

			LVFSDGVisitor = new LocalVariableFlowSensitiveDependencyGraphVisitor(stmt, elseStmt);
			stmt.getRoot().accept(LVFSDGVisitor);
			depGraph = LVFSDGVisitor.getDependencyGraph();

			if (!taken.contains(firstStmt) && (findDependenceIntersection(stmtsWithChangedVarListCache.get(firstStmt),
					firstStmt, dependencies, depGraph)
					|| findDependenceIntersection(getChangedVariables(stmt), stmt, getDepList(firstStmt), depGraph))) {
				dependencyChanged = true;
			} else {
				for (; tempIterationIndex > 0; tempIterationIndex--) {
					tempStmt = flatStatementList.get(tempIterationIndex);
					if (tempStmt == stmt)
						break;
					if (taken.contains(tempStmt))
						continue;

					dependencyChanged = findDependenceIntersection(stmtsWithChangedVarListCache.get(tempStmt), tempStmt,
							dependencies, depGraph)
							|| findDependenceIntersection(getChangedVariables(stmt), stmt, getDepList(firstStmt),
									depGraph);
					if (dependencyChanged)
						break;
				}
			}
			if (!dependencyChanged)
				ret.add(stmt);
			dependencyChanged = false;
		}
		return ret;
	}

	/*
	 * 
	 * @param stmtList1
	 * @param stmtList2
	 * @param outputList
	 * @return boolean value that indicated that a statement is being pulled
	 *         up/pushed down while it prevented another statement form being
	 *         pulled up/pushed down. This means the analysis should be
	 *         repeated. This method compares two lists searching for statements
	 *         that are identical, it verifies both String matching and variable
	 *         binding matching
	 */
	private boolean compareStatementsWithUnchangedDependencies(List<Statement> stmtList1, List<Statement> stmtList2,
			List<IndexPair> outputList) {
		boolean callAgain = false;
		for (Statement thenStmt : stmtList2)
			for (Statement elseStmt : stmtList1)
				if (ConsolidateConditionalUtils.statementsEqual(thenStmt, elseStmt)
						&& thenStmt.subtreeMatch(new BindingMatcher(), elseStmt)) {
					addToList(outputList, elseStmt, thenStmt);
					if (interfered.contains(elseStmt) || interfered.contains(thenStmt)) {
						interfered = new HashSet<Statement>();
						callAgain = true;
					}
					break;
				}
		return callAgain;
	}

	/*
	 * 
	 * @param flatThenStatementList
	 * @param flatElseStatementList
	 * 
	 *            updates the pullUpStatementsLines with identical statements
	 *            from thenStatement and elseStatement, starting from top. Once
	 *            no more statements match another method for disordered matches
	 *            is invoked.
	 */
	private void matchForward(List<Statement> flatThenStatementList, List<Statement> flatElseStatementList) {
		Statement currElseStmt, currThenStmt;
		Iterator<Statement> elseIterator = flatElseStatementList.iterator();
		Iterator<Statement> thenIterator = flatThenStatementList.iterator();

		LocalVariableFlowSensitiveDependencyGraphVisitor LVFSDGVisitor = new LocalVariableFlowSensitiveDependencyGraphVisitor(
				this.ifStmt, false);
		this.ifStmt.getRoot().accept(LVFSDGVisitor);
		Graph<IVariableBinding, DefaultEdge> depGraph = LVFSDGVisitor.getDependencyGraph();

		for (; elseIterator.hasNext() && thenIterator.hasNext();) {
			currElseStmt = (Statement) elseIterator.next();
			currThenStmt = (Statement) thenIterator.next();
			if (!ConsolidateConditionalUtils.statementsEqual(currElseStmt, currThenStmt)
					|| ((currElseStmt instanceof VariableDeclarationStatement)
							&& declarationContainsInitializer((VariableDeclarationStatement) currElseStmt))) {
				while (findForwardDisorderedMatches(currElseStmt, currThenStmt, elseIterator, thenIterator,
						flatElseStatementList, flatThenStatementList, depGraph)) {
					;
				}
				return;
			}
			if (currElseStmt instanceof VariableDeclarationStatement)
				continue;
			if (!(conditionalInterferes(currThenStmt, depGraph) || conditionalInterferes(currElseStmt, depGraph)))
				addToList(pullUpStatementsLines, currElseStmt, currThenStmt);
		}
	}

	private boolean conditionalInterferes(Statement stmt, Graph<IVariableBinding, DefaultEdge> depGraph) {

		return findDependenceIntersection(conditionalChangedVars, null, getDepList(stmt), depGraph)
				|| findDependenceIntersection(getChangedVariables(stmt), stmt, this.conditionalUsedVars, depGraph);
	}

	/*
	 * 
	 * @param stmt
	 * @return whether variable declaration contains an initializer. Variable
	 *         declaration is not pulled up/pushed down, but if it does not
	 *         contain an initializer it can be ignored and not interrupt the
	 *         ordered scan from top to bottom or the from bottom to top
	 */
	private boolean declarationContainsInitializer(VariableDeclarationStatement stmt) {
		for (Object o : stmt.fragments())
			if (o instanceof VariableDeclarationFragment)
				if (((VariableDeclarationFragment) o).getInitializer() != null)
					return true;
		return false;
	}

	/*
	 * 
	 * @param currElseStmt
	 * @param currThenStmt
	 * @param elseIterator
	 * @param thenIterator
	 * @param flatElseStatementList
	 * @param flatThenStatementList
	 * @param firstDepGraph
	 * @return true iff this method should be called again. The algorithm: 1.
	 *         get a list of statements which their dependencies are not
	 *         changed/accessed in the statements before 2. compare lists of
	 *         'then' and 'else' blocks 3. Consider removed matches and recall
	 *         if needed
	 */
	private boolean findForwardDisorderedMatches(Statement currElseStmt, Statement currThenStmt,
			Iterator<Statement> elseIterator, Iterator<Statement> thenIterator, List<Statement> flatElseStatementList,
			List<Statement> flatThenStatementList, Graph<IVariableBinding, DefaultEdge> firstDepGraph) {
		LocalVariableFlowSensitiveDependencyGraphVisitor LVFSDGVisitor = new LocalVariableFlowSensitiveDependencyGraphVisitor(
				currThenStmt, false);
		currThenStmt.getRoot().accept(LVFSDGVisitor);
		Graph<IVariableBinding, DefaultEdge> depGraph = LVFSDGVisitor.getDependencyGraph();

		List<Statement> unchangedElseStmts = getForwardStatementsWithUnchangedDependencies(depGraph, currElseStmt,
				flatElseStatementList.listIterator(flatElseStatementList.indexOf(currElseStmt) + 1),
				flatElseStatementList);
		List<Statement> unchangedThenStmts = getForwardStatementsWithUnchangedDependencies(depGraph, currThenStmt,
				flatThenStatementList.listIterator(flatThenStatementList.indexOf(currThenStmt) + 1),
				flatThenStatementList);
		removeConditionalInterferes(unchangedElseStmts, firstDepGraph);
		removeConditionalInterferes(unchangedThenStmts, firstDepGraph);

		return compareStatementsWithUnchangedDependencies(unchangedElseStmts, unchangedThenStmts,
				pullUpStatementsLines);
	}

	/*
	 * 
	 * @param stmts
	 * @param depGraph
	 * 
	 *            Removes all statements in the stmts list which are dependent
	 *            on variables that might be affected by the conditional
	 *            expression. These statements cannot be pulled up.
	 */
	private void removeConditionalInterferes(List<Statement> stmts, Graph<IVariableBinding, DefaultEdge> depGraph) {
		List<Statement> toRemove = new ArrayList<>();
		for (Statement stmt : stmts)
			if (conditionalInterferes(stmt, depGraph))
				toRemove.add(stmt);
		for (Statement stmt : toRemove)
			stmts.remove(stmt);
	}

	/*
	 * 
	 * @param depGraph
	 * @param firstStmt
	 * @param stmtIterator
	 * @param flatStmtList
	 * @return
	 * 
	 * 		For each statement s iterate all statements above (before) it in
	 *         the flatStatementList and add it to the unchanged list if it none
	 *         of the statements might be changed a variable s depends on. For
	 *         this it is necessary only to consider the dependency graph up to
	 *         the beginning of the IfStatement (improvement- update depGraph
	 *         when a statement is pulled up)
	 */
	private List<Statement> getForwardStatementsWithUnchangedDependencies(Graph<IVariableBinding, DefaultEdge> depGraph,
			Statement firstStmt, Iterator<Statement> stmtIterator, List<Statement> flatStmtList) {

		List<IVariableBinding> dependencies;
		Map<Statement, List<IVariableBinding>> stmtsWithChangedVarListCache = new HashMap<>();
		List<Statement> ret = new ArrayList<>();
		boolean dependencyChanged = false;
		Iterator<Statement> tempIterator = null;// iterates over statements in
												// flatStmtList from firstStmt
												// until stmt
		Statement tempStmt, stmt;

		ret.add(firstStmt);
		if (!stmtIterator.hasNext())
			return ret;
		addStmtChangedListToCache(firstStmt, stmtsWithChangedVarListCache);

		for (; stmtIterator.hasNext();) {
			stmt = stmtIterator.next();
			addStmtChangedListToCache(stmt, stmtsWithChangedVarListCache);
			dependencies = getDepList(stmt);

			tempIterator = ConsolidateConditionalUtils.forwardIterator(firstStmt, flatStmtList);
			assert(tempIterator != null);

			if (!taken.contains(firstStmt) && (findDependenceIntersection(stmtsWithChangedVarListCache.get(firstStmt),
					firstStmt, dependencies, depGraph)
					|| findDependenceIntersection(getChangedVariables(stmt), stmt, getDepList(firstStmt), depGraph))) {
				dependencyChanged = true;
			} else {
				for (; tempIterator.hasNext();) {
					tempStmt = tempIterator.next();
					if (tempStmt == stmt)
						break;
					if (taken.contains(tempStmt))
						continue;

					dependencyChanged = findDependenceIntersection(stmtsWithChangedVarListCache.get(tempStmt), tempStmt,
							dependencies, depGraph)
							|| findDependenceIntersection(getChangedVariables(stmt), stmt, getDepList(firstStmt),
									depGraph);
					if (dependencyChanged)
						break;
				}
			}
			if (!dependencyChanged)
				ret.add(stmt);
			dependencyChanged = false;
		}
		return ret;
	}

	/*
	 * 
	 * @param tempDependencies
	 * @param tempStmt
	 * @param dependencies
	 * @param depGraph
	 * @return true iff tempStmt contains variables that might affect the
	 *         variables in dependencies
	 * 
	 *         The 'interfering' tempStmt is also added to interfered list
	 */
	private boolean findDependenceIntersection(List<IVariableBinding> tempDependencies, Statement tempStmt,
			List<IVariableBinding> dependencies, Graph<IVariableBinding, DefaultEdge> depGraph) {
		for (IVariableBinding var : dependencies) {
			for (IVariableBinding tempVar : tempDependencies)
				if (var == tempVar || (depGraph.containsVertex(var) && depGraph.containsVertex(tempVar)
						&& GraphsUtils.isReachable(var, tempVar, depGraph))) {
					if (tempStmt != null)
						interfered.add(tempStmt);
					return true;
				}
		}
		return false;
	}

	/*
	 * 
	 * @param stmt
	 * @param stmtsWithChangedVarListCache
	 * 
	 *            A list of variables that are actually might be affected by the
	 *            execution of stmt, primitive variables in stmt (without
	 *            side-effect) will be omitted
	 */
	private void addStmtChangedListToCache(Statement stmt,
			Map<Statement, List<IVariableBinding>> stmtsWithChangedVarListCache) {

		List<IVariableBinding> changed = getChangedVariables(stmt);
		stmtsWithChangedVarListCache.put(stmt, changed);
	}

	private List<IVariableBinding> getChangedVariables(Statement stmt) {
		ChangedVariableListerVisitor CVLVisitor = new ChangedVariableListerVisitor();
		stmt.accept(CVLVisitor);
		return CVLVisitor.getChangedVariables();
	}

	/*
	 * 
	 * @param stmt
	 * @return list of all variables that appear in the statement i.e. the
	 *         statement depends on
	 */
	private List<IVariableBinding> getDepList(ASTNode node) {
		DependentVariableListerVisitor DVLVisitor = new DependentVariableListerVisitor();
		node.accept(DVLVisitor);
		return DVLVisitor.getDependentVariables();
	}

	private void addToList(List<IndexPair> listToAdd, Statement currElseStmt, Statement currThenStmt) {
		IndexPair ip = new IndexPair(currElseStmt.getStartPosition(), currThenStmt.getStartPosition(),
				currElseStmt.getLength(), currThenStmt.getLength());
		if (!(pushDownStatementsLines.contains(ip) || pullUpStatementsLines.contains(ip))) {
			listToAdd.add(ip);
			taken.add(currThenStmt);
			taken.add(currElseStmt);
		}
	}

	/**
	 * 
	 * BindingMatcher finds whether the binding of all the children are
	 * identical in order to avoid cases of variables with same name in
	 * different scope
	 *
	 */
	public static class BindingMatcher extends ASTMatcher {
		@Override
		public boolean match(SimpleName node, Object other) {
			if (!(other instanceof SimpleName))
				return false;
			if(node.resolveBinding()==null || node.resolveBinding() == null)
				return false;
			return node.resolveBinding() == ((SimpleName) other).resolveBinding();
		}
	}
}
