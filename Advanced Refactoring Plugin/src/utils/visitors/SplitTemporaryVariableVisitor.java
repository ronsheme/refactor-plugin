package utils.visitors;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.eclipse.jdt.core.dom.AbstractTypeDeclaration;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.IVariableBinding;
import org.eclipse.jdt.core.dom.IfStatement;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.Statement;
import org.eclipse.jdt.core.dom.TypeDeclaration;
import org.jgrapht.DirectedGraph;
import org.jgrapht.graph.DefaultEdge;

import utils.ASTMisc;
import utils.IndexPair;
import utils.VarIndex;
import utils.visitors.LocalVariablesDirectedDependencyGraphVisitor;
import utils.visitors.TempAssignmentCounterVisitor;

/**
 * @author Ron Shemer SplitTemporaryVariableVisitor counts the number of
 *         assignments for each variable. If it is not a collecting temporary
 *         that uses its own value when assigned, and is assigned more than once
 *         it is suggested to be split into two different temporaries
 * 
 *         further improvements: detect whether the temporary begins or stops
 *         being a collecting temporary
 *
 */
public class SplitTemporaryVariableVisitor extends TempAssignmentCounterVisitor {

	private DirectedGraph<IVariableBinding, DefaultEdge> variableDependencyGraph;

	@Override
	public boolean visit(CompilationUnit node) {
		super.visit(node);
		LocalVariablesDirectedDependencyGraphVisitor LVDDGVisitor = new LocalVariablesDirectedDependencyGraphVisitor();
		node.accept(LVDDGVisitor);
		variableDependencyGraph = (DirectedGraph<IVariableBinding, DefaultEdge>) LVDDGVisitor.getDependencyGraph();
		return true;
	}
	
	@Override
	public boolean visit(MethodDeclaration node) {
		setNames(node);
		if (currentMethodBinding != null)
			methodToVariables.put(currentMethodBinding, new ArrayList<IVariableBinding>());
		else
			methodStringToVariables.put(currentMethodString, new ArrayList<>());

		node.getBody().accept(this);
		List<IVariableBinding> vars = (currentMethodBinding == null) ? methodStringToVariables.get(currentMethodString)
				: methodToVariables.get(currentMethodBinding);
		for (IVariableBinding var : vars)
			if ((variablesToAssignments.get(var).size() > 1 || (variablesToAssignments.get(var).size() == 1
					&& (variablesToAssignments.get(var).get(0).getParent() instanceof IfStatement)
					&& countConditionalAssignment((IfStatement) variablesToAssignments.get(var).get(0).getParent(),
							var) > 1))
					&& var != null)
				// if variable is not using itself at any point (can be improved
				// to be more precise)
				if (!variableDependencyGraph.containsEdge(var, var) && !allSameStmt(variablesToAssignments.get(var))) {
					tempStringsToRefactor.add(var.getName() + " in method " + currentMethodString + "\n");
					Statement s = ASTMisc.getStatementParent(variablesToAssignments.get(var).get(0));
					tempPositionsToRefactor.add(new VarIndex(s.getStartPosition(), s.getLength(),var.getName()));
				}
		return false;
	}
	//some minor case I came across
	private boolean allSameStmt(List<Expression> list) {
		for (Expression e1 : list)
			for (Expression e2 : list)
				if (!(e1.toString().equals(e2.toString())
						&& e1.subtreeMatch(new ConsolidateConditionalVisitor.BindingMatcher(), e2)))
					return false;
		return true;
	}
}
