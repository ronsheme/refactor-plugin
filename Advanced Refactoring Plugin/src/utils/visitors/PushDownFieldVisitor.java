package utils.visitors;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.eclipse.jdt.core.IPackageFragmentRoot;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.ReturnStatement;
import org.eclipse.jdt.core.dom.SimpleName;
import org.eclipse.jdt.core.dom.SingleVariableDeclaration;
import org.eclipse.jdt.core.dom.Statement;
import org.eclipse.jdt.core.dom.VariableDeclarationFragment;
import org.eclipse.jdt.core.dom.VariableDeclarationStatement;

import utils.CompilationUtils;
import utils.CompilationPaths;
import utils.inheritanceUtils;

/**
 * In some class hierarchies, although it was planned to use a field universally 
 * for all classes, in reality the field is used only in some subclasses. This 
 * situation can occur when planned features fail to pan out, for example.
 * This can also occur due to extraction (or removal) of part of the functionality 
 * of class hierarchies. Pushing down such fields improves internal class 
 * coherence - a field is located where it is actually used.
 * This class implements an automatic Push Down Field, by scanning projects's
 * hierarchy and finding the correct location for each class field.
 */
public class PushDownFieldVisitor extends ASTVisitor {
	
	/* Holds the source path to the analyzed class */
	private String srcPath;
	
	/* Holds a CompilationPaths objects who clusters project paths */
	private CompilationPaths compPath;
	
	/* Holds class's fields */
	private FieldRecord[] classFields;
	
	/* Holds the class under analysis */
	private Class<?> analysedClass;
	
	/* Holds Class Methods */
	List<MethodDeclaration> methods = new LinkedList<MethodDeclaration>();
	
	/* Holds Class variable declarations */
	List<VariableDecRecord> varDeclarations = new LinkedList<VariableDecRecord>();
	
	/* Holds all class's simple names as derived from the AST */
	private List<SimpleName> simpleNames = new LinkedList<SimpleName>();
	
	/* Holds a mapping between method names and method bodies */
	private Map<String, List<Statement>> methodBodies;
	
	/* Holds all class's names as derived from the AST */
	private List<NameRecord> names = new LinkedList<NameRecord>();
	
	/* Holds the result of the analysis. Maps each field to the list 
	   of classes it should be pushed down to */
	private static Map<FieldRecord, List<Class<?>>> analysisResult = 
			new HashMap<FieldRecord, List<Class<?>>>();
	
	/* 'static' keyword, 'get' and 'set' constants */
	private static final String STATIC_CONSTANT = " static ";
	
	public void resetAnalysisResult()
	{
		analysisResult = 
				new HashMap<FieldRecord, List<Class<?>>>();
	}
	
	/*
	 * This private constructor is called by the public constructor 
	 * and is vital for collecting class's relevant fields and for
	 * the recursive process of determining the most deep classes in
	 * the hierarchy a field should be pushed down to.
	 */
	private PushDownFieldVisitor(Class<?> c, FieldRecord field, CompilationPaths compPath) {
		this.compPath=compPath;
		try {
			for (IPackageFragmentRoot f : compPath.javaProject.getAllPackageFragmentRoots()) {
				if(f.getKind() == IPackageFragmentRoot.K_SOURCE)
					srcPath = 
						compPath.javaProject.getProject().getLocation().toString()+"/src";
			}
		} catch (JavaModelException e) {
			e.printStackTrace();
		}
		analysedClass = c;
		classFields = relevantClassFields(c,field);
		methodBodies = new HashMap<String, List<Statement>>();
	}
	
	/**
	 * Creates a PushDownFieldVisitor object. Run the analysis on the given class.
	 * @param c the class to run the Push Down Field analysis on.
	 * @param compPath the relevant paths for analysis
	 */
	public PushDownFieldVisitor(Class<?> c, CompilationPaths compPath) {
		this(c, null, compPath);
}
	
	/*
	 * Auxiliary for constructor. Ensures that only class fields that are
	 * relevant for the Push Down Field analysis are added to 'classFields'. 
	 */
	private FieldRecord[] relevantClassFields(Class<?> c, FieldRecord optionalField) {
		List<FieldRecord> relevantFields = new LinkedList<FieldRecord>();
		/* Get all fields as defined in class */
		Field[] declaredFields = c.getDeclaredFields();
		/* 
		 * Loop through fields, create a 'FieldRecord' for each field and 
		 * add it to the array holding the fields. Exclude static fields. 
		 */
		for (int i = 0; i < declaredFields.length; i++) {
			String field = declaredFields[i].toString();
			if(!field.contains(STATIC_CONSTANT)) {
				String fieldName = declaredFields[i].getName();
				String fieldType = declaredFields[i].getType().getSimpleName();
				List<String> appearsInMethods = new LinkedList<String>();
				FieldRecord record = new FieldRecord(fieldName, fieldType, appearsInMethods, null);
				relevantFields.add(record);
			}
		}
		/* 
		 * It is legitimate to add a field to 'classFields' (hence "pushing it down")
		 * only if there is no field with the same name at the extending class.
		 */
		if(optionalField != null && fieldNotExist(optionalField, relevantFields)) {
		relevantFields.add(optionalField);
		}
		return relevantFields.toArray(new FieldRecord[0]);
	}
	
	/**
	 * Run the Push Down Field analysis.
	 * @return the analysis report as string
	 * @throws ClassNotFoundException
	 */
	public String runClassAnalysis() throws ClassNotFoundException {
		String result = "";
		/* Get AST */
		CompilationUnit ast = CompilationUtils.getCompilationUnit(compPath);
		ast.accept(this);
		/* Collect class information from AST */
		names();
		namesInMethodBodies();
		collectAllGettersAndSetters();
		CollectMethodListsForFields();
		
		/* Analysis irrelevant for classes with no fields */
		if(classFields.length == 0) {
			result = "Push Down Field analysis is irrelevant for"
					+ " classes without any fields. Please select a class"
					+ " with one or more fields and try again.";
			return result;
		}
		/* Analysis irrelevant for classes with no fields */
		List<Class<?>> inheritsFromClass = 
			inheritanceUtils.inheritsFromClass(this.analysedClass,srcPath);
		if(inheritsFromClass.isEmpty()) {
			result = "Push Down Field analysis is irrelevant for"
					+ " classes without any extending classes. Please select"
					+ " a class with extending classes and try again.";
			return result;
		}
		/* 
		 * For each field, find the correct class down the hierarchy it should be 
		 * pushed down to. Treat special cases:
		 * 1. the field is defined and used in the root (parameter) class. 
		 * 2. the field is never in use - suggested to be deleted.
		 */
		for (int i = 0; i < classFields.length; i++) {
		FieldRecord field = classFields[i];
		projectTreeSingleFieldWalk(field, this); 
	}	
		return toString(analysisResult);
}

	
	/*
	 * Getter functions must be handled because calling a getter is 
	 * equivalent to referring the field itself in our analysis.
	 * Given a field and a method, checks whether or not the method is
	 * a legitimate getter for the field. If so, update the information
	 * over the given field.
	 */
	private void findGetter(MethodDeclaration md, FieldRecord field) {
		int numOfStatements = md.getBody().statements().size();
		int numOfParameters = md.parameters().size();
		if(numOfStatements != 1 || numOfParameters != 0)
			return;
		
		Statement s = (Statement) md.getBody().statements().get(0);
		if(s.toString().startsWith("return ")) {
			ReturnStatement rs = (ReturnStatement) s;
			String returnedValue = rs.getExpression().toString();
			String fieldName = field.fieldName;
			String fullNameOfField = "this." + fieldName;
			if(returnedValue.equals(fieldName) || returnedValue.equals(fullNameOfField)) {
				field.fieldGetter = md;
			}
		}
	}
	
	/*
	 * Setter functions must be handled because calling a setter is 
	 * equivalent to referring the field itself in our analysis.
	 * Given a field and a method, checks whether or not the method is
	 * a legitimate setter for the field. If so, update the information
	 * over the given field.
	 */
	private void findSetter(MethodDeclaration md, FieldRecord field) {
		int numOfStatements = md.getBody().statements().size();
		int numOfParameters = md.parameters().size();
		if(numOfStatements != 1 || numOfParameters != 1)
			return;
		
		SingleVariableDeclaration parameter = (SingleVariableDeclaration) md.parameters().get(0);
		String paramType = parameter.getType().toString();
		String fieldType = field.type;
		if(!paramType.equals(fieldType))
			return;
		
		Statement s = (Statement) md.getBody().statements().get(0);
		if(!setterMatcher(s.toString()))
			return;
		
		String fieldName = field.fieldName;		
		if(!fieldName.equals(extractLHS(s.toString())) &&
				!("this." + fieldName).equals(extractLHS(s.toString())))
			return;
		
		String parameterIdentifier =
				((SingleVariableDeclaration)md.parameters().get(0)).getName().getIdentifier();
		String rhs = extractRHS(s.toString());
		if(rhs.equals(parameterIdentifier)) {
			field.fieldSetter = md;
		}
	}
	
	/* Extract the Right-Hand-Side of an assignment */
	private static String extractRHS(String assignment) {
		int indexOfEquality = assignment.indexOf('=');
		String untilEquality = assignment.substring(indexOfEquality + 1);
		String firstTrim = untilEquality.trim();
		return firstTrim.substring(0, firstTrim.length()-1);
	}
	
	/* Extract the Left-Hand-Side of an assignment */
	private static String extractLHS(String assignment) {
		int indexOfEquality = assignment.indexOf('=');
		String untilEquality = assignment.substring(0, indexOfEquality);
		return untilEquality.trim();
	}
	
	/* Checks that the setter has a reasonable pattern */
	public boolean setterMatcher(String s){
		return s.matches("(this.|^$)[a-z_$][a-z_$0-9]*\\s*=\\s*[a-z_$][a-z_$0-9]*[\\s;]*;[\\s;]*");
	}
	
	
	/* 
	 * Auxiliary function. Given a field and a list of fields, it returns
	 * true if and only if the list does not contain an object representing
	 * the same field as the passed field. 
	 */
	private static boolean fieldNotExist(FieldRecord field, List<FieldRecord> fields) {
		for (int i = 0; i < fields.size(); i++) {
			FieldRecord curr = fields.get(i);
			if(curr.equals(field))
				return false;
		}
		return true;
	}
	
	/* This visitor collects all class's simple names into 'simpleNames' */
	public boolean visit(SimpleName node) {
		simpleNames.add(node);
		return false;	
	}
	
	/* This visitor collects all class's variable declarations into 'varDeclarations' */
	public boolean visit(VariableDeclarationStatement node) {
		if(node.getParent() != null && node.getParent().getParent() != null) {
			Object gp = node.getParent().getParent();
			if(gp instanceof MethodDeclaration) {
				VariableDecRecord vr = new VariableDecRecord(node, (MethodDeclaration) gp);
				varDeclarations.add(vr);
			}
		}
		return false;
	}
	
	/* 
	 * This visitors collects all class's function bodies, and constructs
	 * the 'methodBodies' mapping between function names and function bodies. 
	 */
	public boolean visit(MethodDeclaration node) {
		@SuppressWarnings("unchecked")
		List<Statement> body = node.getBody().statements();
		String methodName = node.getName().toString();
		if(!node.isConstructor())
			methodBodies.put(methodName, body);
		methods.add(node);
		return false;
	}
	
	/* Calling the visitor for each statement in a function body */
	private void names() {
		for (String method : methodBodies.keySet()) {
			List<Statement> body = methodBodies.get(method);
			for (Statement s : body) {
				s.accept(this);
			}	
		}
	}
		
	/* 
	 * The goal of this method is to filter out names from the class 
	 * that are not part of a body of a method.
	 */
	private void namesInMethodBodies() {
		/* Find method declaration ancestor	*/
		for (SimpleName name : simpleNames) {
			MethodDeclaration md = hasMethodDeclarationAncestor(name);
			/* Name is not in a method body */
			if (md == null)
				continue;
			/* Name is in a method body, add to the list */
			NameRecord nr = new NameRecord(name.toString(), md.getName().toString(), name);
			names.add(nr);
		}
	}
	
	/* 
	 * Auxiliary for namesInMethodBodies. Given an AST node, the function
	 * returns the MethodDeclaration ancestor of the node or null if such
	 * does not exist. Note: a MethodDeclaration object represents a 
	 * method in a Java class.
	 */
	private static MethodDeclaration hasMethodDeclarationAncestor(ASTNode node) {
		if(node == null) 
			return null;
		/* Bottom up tree scan */
		ASTNode parent = node.getParent();
		while(parent != null) {
			if(parent instanceof MethodDeclaration) {
				return (MethodDeclaration) parent;
			}
			parent = parent.getParent();
		}
		return null;
	}
	
	/* Run through class fields, update getter and setter functions (if exist) */
	private void collectAllGettersAndSetters() {
		for (int i = 0; i < classFields.length; i++) {
			for (MethodDeclaration method : methods) {
				findGetter(method, classFields[i]);
				findSetter(method, classFields[i]);
			}
		}
	}
	
	/* Collects all variable declared in class's functions */
	private List<VariableRecord> collectAllVarDeclarations() {
		List<VariableRecord> allVariables = new LinkedList<VariableRecord>();
		for (VariableDecRecord vdr : varDeclarations) {
			MethodDeclaration md = vdr.parentMethod;
			for (Object o : vdr.var.fragments()) {
				String name = ((VariableDeclarationFragment)o).getName().toString();
				String parentMethod = md.getName().toString();
				VariableRecord vr = new VariableRecord(name, parentMethod, vdr.var);
				allVariables.add(vr);
			}
		}
		return allVariables;
	}
	
	/* 
	 * Checks if a variable with the same name of a certain field is declared
	 * before referring that name, concluding that there is no field access. 
	 */
	private boolean varDeclarationCheck(FieldRecord fr, NameRecord nr, List<VariableRecord> varDecs) {
		String name = nr.name;
		String parentMethod = nr.parentMethod;
		for (VariableRecord vr : varDecs) {
			String varName = vr.varName;
			String varParent = vr.parentMethod;
			if(name.equals(varName) && parentMethod.equals(varParent) && fr.fieldName.equals(varName)) {
				int startPositionVar = vr.var.getStartPosition();
				int startPositioName = nr.nameObject.getStartPosition();		
				if(startPositionVar < startPositioName) {
					return true;
				}
			}		
		}
		return false;
	}
	
	
	/*
	 * constructs a mapping between field and the list of methods
	 * doing any kind of use in that field. If the list of methods 
	 * is not empty, it also updates the isInUse field to be true.
	 */
	private void CollectMethodListsForFields() {
		List<VariableRecord> varDecs = collectAllVarDeclarations();
		
		for (int i = 0; i < classFields.length; i++) {
			FieldRecord field = classFields[i];
			String fieldName = field.fieldName;
			List<String> methods = new LinkedList<String>();
			String fieldGetterName = "";
			String fieldSetterName = "";
			if (field.fieldGetter != null)
				fieldGetterName = field.fieldGetter.getName().toString();
			if (field.fieldSetter != null)
				fieldSetterName = field.fieldSetter.getName().toString();

			for (NameRecord simpleName : names) {
				String currName = simpleName.name;
				String parentMethod = simpleName.parentMethod;
				if (parentMethod.equals(fieldGetterName) || parentMethod.equals(fieldSetterName))
					continue;
				if(varDeclarationCheck(field, simpleName, varDecs))
					continue;
				if (currName.equals(fieldName) || fieldGetterName.equals(currName)
						|| fieldSetterName.equals(currName)) {
					methods.add(parentMethod);
				}
			}
			field.appearsInMethods = methods;
			field.isInUse = !methods.isEmpty();
			if (field.isInUse)
				field.mostDeepUse = this.analysedClass;
		}
	}
	
	/* Constructs an analysis report (as string) */
	private String toString(Map<FieldRecord, List<Class<?>>> analysis) {
		String result = "Push Down Field analysis ended with the following results:\n";
		if (allFieldsUsedInTopClass(analysis)) {
			result = result + "All class fields are in use, hence cannot be pushed down.\n";
			return result;
		}
		List<FieldRecord> usedInTop = fieldsInUseInTopClass(analysis);
		if (!usedInTop.isEmpty()) {
			result = result + "\nFields that are in use, and need not be pushed down:\n[";
			for (int i = 0; i < usedInTop.size(); i++) {
				if (i == usedInTop.size() - 1) {
					result += usedInTop.get(usedInTop.size() - 1).fieldName + "]\n";
				} else {
					result += usedInTop.get(i).fieldName + ",";
				}
			}
		}
		List<FieldRecord> toPush = fieldsToBePushedDown(analysis);
		if (!toPush.isEmpty()) {
			result = result
					+ "\nFields that are suggested to be pushed down "
					+ "(and the corresponding list of classes to be pushed down to):\n";
			for (FieldRecord field : toPush) {
				String fieldName = field.fieldName;
				List<Class<?>> classes = analysis.get(field);
				result += "The field " + fieldName + " is suggested to be pushed down to [";
				for (int i = 0; i < classes.size(); i++) {
					if (i == classes.size() - 1) {
						result += classes.get(classes.size() - 1).getSimpleName() + "]\n";
					} else {
						result += classes.get(i).getSimpleName() + ",";
					}
				}
			}
		}
		List<FieldRecord> toDelete = fieldsToBeDeleted();
		if (!toDelete.isEmpty()) {
			result = result + "\nFields that are not used in the hierarchy"
					+ " and are suggested to be deleted:\n[";
			for (int i = 0; i < toDelete.size(); i++) {
				if (i == toDelete.size() - 1) {
					result += toDelete.get(toDelete.size() - 1).fieldName + "]\n";
				} else {
					result += toDelete.get(i).fieldName + ",";
				}
			}
		}
		return result;
	}
	
	/* Handles the case in which the field is in use at the analyzed class */
	private List<FieldRecord> fieldsInUseInTopClass(Map<FieldRecord, List<Class<?>>> analysis) {
		List<FieldRecord> result = new LinkedList<FieldRecord>();
		for (FieldRecord fr : analysis.keySet()) {
			List<Class<?>> classes = analysis.get(fr);
			if(classes.size() == 1 && classes.get(0) == analysedClass) {
				result.add(fr);
			}
		}
		return result;
	}
	
	/* Handles the case in which the field should be pushed down the hierarchy */
	private List<FieldRecord> fieldsToBePushedDown(Map<FieldRecord, List<Class<?>>> analysis) {
		List<FieldRecord> result = new LinkedList<FieldRecord>();
		for (FieldRecord fr : analysis.keySet()) {
			List<Class<?>> classes = analysis.get(fr);
			if(!classes.isEmpty() && classes.get(0) != analysedClass) {
				result.add(fr);
			}
		}
		return result;
	}
	
	/* Handles the case in which the field is not in use at all */
	private List<FieldRecord> fieldsToBeDeleted() {
		List<FieldRecord> result = new LinkedList<FieldRecord>();
		for (int i = 0; i < classFields.length; i++) {
			FieldRecord fr = classFields[i];
			if(fr.mostDeepUse == null) {
				result.add(fr);
			}
		}
		return result;
	}
	
	/* Checks if all class fields are in use at the analyzed class */
	private boolean allFieldsUsedInTopClass(Map<FieldRecord, List<Class<?>>> analysis) {
		return classFields.length == fieldsInUseInTopClass(analysis).size();
	}
	
	/*
	 * The goal of the FieldRecord nested class is to create objects
	 * representing fields. A FieldRecord holds an important
	 * information about each field. 
	 */
	private static class FieldRecord {
		
		/* Holds the simple name of the field */
		private String fieldName;
		
		/* Holds the type of the field */
		private String type;
		
		/* Holds the list of methods in which the field appears */
		@SuppressWarnings("unused")
		private List<String> appearsInMethods;
		
		/* The value is true if and only if at least one method uses the field */
		private boolean isInUse = false;
		
		/* Holds the deepest class in the project hierarchy in which
		 * the field should be defined */
		private Class<?> mostDeepUse;
		
		/* Holds getter name if one exist or null otherwise */
		private MethodDeclaration fieldGetter = null;
		
		/* Holds setter name if one exist or null otherwise */
		private MethodDeclaration fieldSetter = null;
		
		/* Constructor */
		private FieldRecord(String fieldName, String type, List<String> appearsInMethods,
				Class<?> mostDeepUse) {
			this.fieldName = fieldName;
			this.type = type;
			this.appearsInMethods = appearsInMethods;
			this.mostDeepUse = mostDeepUse;
		}			
		
		/* Two FieldRecord objects are equal if and only if they have the same name */
		@Override
		public boolean equals(Object other) {
			if(!(other instanceof FieldRecord))
				return false;
			return this.fieldName.equals(((FieldRecord) other).fieldName);		
		}
		
		/* Equal FieldRecords gets the same hash code value */
		@Override
		public int hashCode() {
			return new HashCodeBuilder(17, 31).append(fieldName).toHashCode();
		}
	}
	
	/*
	 * The goal of the NameRecord nested class is to create objects
	 * representing names. A NameRecord holds, in addition to it's
	 * name, the name of the method it appears in. 
	 */
	public class NameRecord {
		
		/* Holds the name referenced by the SimpleName object */
		public String name;
		
		/* The name of the method this name located in */
		public String parentMethod;
		
		/* The ASTNode object */
		public SimpleName nameObject;
		
		/* Constructor */
		public NameRecord(String name, String parentMethod, SimpleName nameObject) {
			this.name = name;
			this.parentMethod = parentMethod;
			this.nameObject = nameObject;
		}
	}
	
	/* 
	 * A VariableDecRecord holds an important information about any
	 * variable declaration
	 */
	public class VariableDecRecord {
		
		private VariableDeclarationStatement var;
		private MethodDeclaration parentMethod;
		
		public VariableDecRecord(VariableDeclarationStatement var, MethodDeclaration parentMethod) {
			this.var = var;
			this.parentMethod = parentMethod;
		}
	}
	
	/* A VariableRecord holds an important information about variables */
	public class VariableRecord {
		
		private String varName;
		private String parentMethod;
		private VariableDeclarationStatement var;
		
		public VariableRecord(String varName, String parentMethod, VariableDeclarationStatement var) {
			this.varName = varName;
			this.parentMethod = parentMethod;
			this.var = var;
		}
	}
	
	/*
	 * projectTreeSingleFieldWalk is a recursive function that given a field and a class
	 * in the hierarchy, implements a class hierarchy tree walk for a single field.
	 * At the end of each tree walk, we have the analysis results for this specific 
	 * field.
	 */
	private static void projectTreeSingleFieldWalk(FieldRecord field, PushDownFieldVisitor top)
			throws ClassNotFoundException {
		/* Deepest spot of use for this field located  */
		if(field.isInUse) {
			field.mostDeepUse = top.analysedClass;
			if(analysisResult.containsKey(field)) {
				List<Class<?>> targetClasses = analysisResult.get(field);
				targetClasses.add(field.mostDeepUse);
			} else {
				List<Class<?>> classes = new LinkedList<Class<?>>();
				classes.add(field.mostDeepUse);
				analysisResult.put(field, classes);
			}
			return;
		}
		List<Class<?>> inheritsFromClass = 
				inheritanceUtils.inheritsFromClass(top.analysedClass, top.srcPath);
		
		for (Class<?> extendingClass : inheritsFromClass) {
			CompilationUnit ast = CompilationUtils.getCompilationUnit(new CompilationPaths
					("", null, top.srcPath+"/"+extendingClass.getPackage().getName()+"/"
				+ extendingClass.getSimpleName() + ".java",null));
			
			PushDownFieldVisitor p = new PushDownFieldVisitor(extendingClass, field, top.compPath);
			ast.accept(p);
			
			p.names();
			p.namesInMethodBodies();
			p.collectAllGettersAndSetters();
			p.CollectMethodListsForFields();
			
			/* Recursive call */
			projectTreeSingleFieldWalk(field, p);
	}
}
	
}
