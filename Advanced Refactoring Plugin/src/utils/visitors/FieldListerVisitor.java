package utils.visitors;

import java.util.ArrayList;
import java.util.List;
import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.FieldDeclaration;
import org.eclipse.jdt.core.dom.IVariableBinding;
import org.eclipse.jdt.core.dom.TypeDeclaration;
import org.eclipse.jdt.core.dom.VariableDeclarationFragment;

/**
 * 
 * FieldListerVisitor lists all the fields in a class
 *
 */
public class FieldListerVisitor extends ASTVisitor {

	List<VariableDeclarationFragment> FieldDeclaration = new ArrayList<VariableDeclarationFragment>();
	List<IVariableBinding> FieldBindings = new ArrayList<>();
	boolean inFirstDecl = true;

	public List<IVariableBinding> getFieldBindings() {
		return FieldBindings;
	}

	public List<VariableDeclarationFragment> getFieldDeclarations() {
		return FieldDeclaration;
	}

	@Override
	public boolean visit(FieldDeclaration node) {
		for (Object o : node.fragments())
			FieldDeclaration.add((VariableDeclarationFragment) o);
		return false;
	};

	/**
	 * visit(TypeDeclaration node): if this is not the first visit it means
	 * inner class, no need to continue
	 */
	@Override
	public boolean visit(TypeDeclaration node) {
		if (inFirstDecl) {
			inFirstDecl = false;
			return super.visit(node);
		}
		return false;
	}
}