package utils.visitors;

import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.IVariableBinding;
import org.jgrapht.graph.DefaultDirectedGraph;
import org.jgrapht.graph.DefaultEdge;

public abstract class FlowInsensitiveDirectedDependencyGraphVisitor extends FlowInsensitiveDependencyGraphVisitor {
		
	@Override
	public boolean visit(CompilationUnit node) {
		//a=0;
		 g = new DefaultDirectedGraph<IVariableBinding, DefaultEdge>(DefaultEdge.class) ;
		 return super.visit(node);
	}
}
