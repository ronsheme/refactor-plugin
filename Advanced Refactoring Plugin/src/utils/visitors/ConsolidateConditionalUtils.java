package utils.visitors;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.jdt.core.dom.Block;
import org.eclipse.jdt.core.dom.Statement;

/**
 * 
 * @author Ron Shemer ConsolidateConditionalUtils class contains several
 *         utilities used by ConsolidateConditionalVisitor
 *
 */
public class ConsolidateConditionalUtils {
	/**
	 * 
	 * @param o
	 *            object, should be a statement in the list l
	 * @param l
	 *            the list that will be iterated
	 * @return an iterator for the list l with current index points to the
	 *         parameter o, this means that on the following use of .next() the
	 *         successor of o in the list will be returned
	 */
	public static Iterator<Statement> forwardIterator(Object o, List<Statement> l) {
		Iterator<Statement> iter = l.iterator();
		for (; iter.hasNext();)
			if (iter.next() == o)
				return iter;
		return null;
	}

	/**
	 * 
	 * @param s1
	 * @param s2
	 * @return perform comparison of the strings which the statements originate
	 *         from
	 */
	public static boolean statementsEqual(Statement s1, Statement s2) {
		return s1.toString().equals(s2.toString());
	}

	/**
	 * 
	 * @param stmt
	 * @return a list of statements from the parameter, whether it is the
	 *         statements of a Block or the statement wrapped as a list
	 */
	public static List<Statement> getStatementsAsList(Statement stmt) {
		List<Statement> stmtList;
		if (stmt instanceof Block)
			return ((Block) stmt).statements();
		stmtList = new ArrayList<Statement>();
		stmtList.add(stmt);
		return stmtList;
	}

	/**
	 * 
	 * @param stmt
	 * @return recursively flatten all the blocks in a statement such that any
	 *         element in the returned list is a non-block statement
	 */
	public static List<Statement> getFlatStatement(Statement stmt) {
		List<Statement> flatStmtList = new ArrayList<Statement>();
		if (stmt instanceof Block) {
			Block stmtBlock = (Block) stmt;
			for (Iterator<Statement> iterator = stmtBlock.statements().iterator(); iterator.hasNext();)
				flatStmtList.addAll(getFlatStatement((Statement) iterator.next()));
		} else
			flatStmtList.add(stmt);
		return flatStmtList;
	}
}
