package utils.visitors;

import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.ArrayCreation;
import org.eclipse.jdt.core.dom.ClassInstanceCreation;
import org.eclipse.jdt.core.dom.PostfixExpression;
import org.eclipse.jdt.core.dom.PrefixExpression;

/**
 * 
 * @author Ron Shemer DetectSideEffectVisitor should be used on a statement,
 *         changes foundSideEffect to true iff it finds a ++/-- prefix/postfix
 *         operand
 *
 */
public class DetectSideEffectVisitor extends ASTVisitor {

	private boolean foundSideEffect = false;

	@Override
	public boolean visit(ArrayCreation node) {
		foundSideEffect = true;
		return false;
	}

	@Override
	public boolean visit(ClassInstanceCreation node) {
		foundSideEffect = true;
		return false;
	}

	@Override
	public boolean visit(PrefixExpression node) {
		if (node.getOperator() == PrefixExpression.Operator.DECREMENT
				|| node.getOperator() == PrefixExpression.Operator.INCREMENT)
			foundSideEffect = true;
		return false;
	}

	@Override
	public boolean visit(PostfixExpression node) {
		foundSideEffect = true;
		return false;
	}

	public boolean isFoundSideEffect() {
		return foundSideEffect;
	}
}
