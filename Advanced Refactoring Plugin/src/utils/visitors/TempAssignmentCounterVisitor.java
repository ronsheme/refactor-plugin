package utils.visitors;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.AbstractTypeDeclaration;
import org.eclipse.jdt.core.dom.Assignment;
import org.eclipse.jdt.core.dom.Block;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.IBinding;
import org.eclipse.jdt.core.dom.IMethodBinding;
import org.eclipse.jdt.core.dom.IVariableBinding;
import org.eclipse.jdt.core.dom.IfStatement;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.PostfixExpression;
import org.eclipse.jdt.core.dom.PrefixExpression;
import org.eclipse.jdt.core.dom.SimpleName;
import org.eclipse.jdt.core.dom.TypeDeclaration;
import org.eclipse.jdt.core.dom.VariableDeclarationFragment;
import org.eclipse.jdt.core.dom.VariableDeclarationStatement;

import utils.VarIndex;
import utils.visitors.FieldListerVisitor;

/**
 * 
 * @author Ron Shemer TempAssignmentCounterVisitor keeps for each local variable
 *         or field all the assignments to it
 *
 */
public abstract class TempAssignmentCounterVisitor extends ASTVisitor {

	protected IMethodBinding currentMethodBinding;
	protected String currentMethodString;
	protected Map<String, List<IVariableBinding>> methodStringToVariables;
	protected Map<IMethodBinding, List<IVariableBinding>> methodToVariables;
	protected Map<IVariableBinding, List<Expression>> variablesToAssignments;
	protected List<IVariableBinding> fieldVariables;
	protected List<String> tempStringsToRefactor;
	protected List<VarIndex> tempPositionsToRefactor;
	protected Map<IfStatement, Map<IVariableBinding, Integer>> thenBlockAssignment = new HashMap<>();
	protected Map<IfStatement, Map<IVariableBinding, Integer>> elseBlockAssignment = new HashMap<>();
	protected boolean inIfBlock;
	protected boolean inElseBlock;
	protected IfStatement currIfStmt;
	protected IfStatement rootIfStmt;
	protected Map<IfStatement, List<IfStatement>> childIfStmtOfThenIfStmt = new HashMap<>();
	protected Map<IfStatement, List<IfStatement>> childIfStmtOfElseIfStmt = new HashMap<>();
	private boolean countInitializations = true;// switch
	private boolean inInitialization = false;

	public void setCountInitializations(boolean countInitializations) {
		this.countInitializations = countInitializations;
	}

	public List<VarIndex> getTempPositionsToRefactor() {
		return tempPositionsToRefactor;
	}

	/**
	 * 
	 * @param ifStmt
	 * @param var
	 * @return this method counts assignments of variables in conditionals. if
	 *         there is no more than one assignment in each of the conditional
	 *         blocks it returns 1 (or 0). the analysis starts from the most
	 *         inner conditional and results are passed recursively
	 */
	public int countConditionalAssignment(IfStatement ifStmt, IVariableBinding var) {
		int totalThen = 0;
		int totalElse = 0;
		if (thenBlockAssignment.get(ifStmt) != null && thenBlockAssignment.get(ifStmt).containsKey(var))
			totalThen += thenBlockAssignment.get(ifStmt).get(var);
		if (elseBlockAssignment.get(ifStmt) != null && elseBlockAssignment.get(ifStmt).containsKey(var))
			totalElse += elseBlockAssignment.get(ifStmt).get(var);
		if (totalThen <= 1) {
			for (IfStatement is : childIfStmtOfThenIfStmt.get(ifStmt))
				totalThen += countConditionalAssignment(is, var);
			if (totalThen > 1)
				return totalThen + totalElse;
			if (totalElse <= 1) {
				for (IfStatement is : childIfStmtOfElseIfStmt.get(ifStmt))
					totalElse += countConditionalAssignment(is, var);
				if (totalElse > 1)
					return totalElse + totalThen;
				if (totalElse + totalThen == 2)
					return 1;
				return totalElse + totalThen;
			} else
				return totalThen + totalElse;
		} else
			return totalThen + totalElse;

	}

	public List<String> getTempsToRefactor() {
		return tempStringsToRefactor;
	}
	
	@Override
	public boolean visit(TypeDeclaration node) {
		if(node.isInterface())
			return false;
		return super.visit(node);
	}
	
	protected void addCount(IVariableBinding varBinding, Expression rhsExpression) {
		List<Expression> nodeList;

		nodeList = initialVarList(varBinding);

		if (inInitialization && !countInitializations)
			return;
		if (currIfStmt != null) {
			addToRightBlock(varBinding, currIfStmt);
			if (currIfStmt == rootIfStmt)
				nodeList.add(rhsExpression);
			else if (!(thenBlockAssignment.get(rootIfStmt).containsKey(varBinding)
					|| elseBlockAssignment.get(rootIfStmt).containsKey(varBinding))) {
				nodeList.add(rhsExpression);
				addToRightBlock(varBinding, rootIfStmt);
			}
		} else
			nodeList.add(rhsExpression);
	}

	private List<Expression> initialVarList(IVariableBinding varBinding) {
		List<Expression> nodeList;
		if (variablesToAssignments.containsKey(varBinding))
			nodeList = variablesToAssignments.get(varBinding);

		else {
			nodeList = new ArrayList<Expression>();
			variablesToAssignments.put(varBinding, nodeList);
		}
		return nodeList;
	}

	private void addToRightBlock(IVariableBinding varBinding, IfStatement ifStmt) {
		if (inIfBlock) {
			if (!thenBlockAssignment.get(ifStmt).containsKey(varBinding))
				thenBlockAssignment.get(ifStmt).put(varBinding, 0);
			Integer temp = thenBlockAssignment.get(ifStmt).get(varBinding);
			thenBlockAssignment.get(ifStmt).put(varBinding, temp + 1);
		} else {
			if (!elseBlockAssignment.get(ifStmt).containsKey(varBinding))
				elseBlockAssignment.get(ifStmt).put(varBinding, 0);
			Integer temp = elseBlockAssignment.get(ifStmt).get(varBinding);
			elseBlockAssignment.get(ifStmt).put(varBinding, temp + 1);
		}
	}

	@Override
	public boolean visit(PrefixExpression node) {
		SimpleName tmpSimpleName;

		if (node.getOperator() == PrefixExpression.Operator.DECREMENT
				|| node.getOperator() == PrefixExpression.Operator.INCREMENT) {
			if (node.getOperand() instanceof SimpleName) {
				tmpSimpleName = ((SimpleName) node.getOperand());
				if (tmpSimpleName.resolveBinding() instanceof IVariableBinding)
					addCount((IVariableBinding) tmpSimpleName.resolveBinding(), node);
			}
		} else
			return super.visit(node);
		return false;
	}

	@Override
	public boolean visit(PostfixExpression node) {
		SimpleName tmpSimpleName;
		if (node.getOperand() instanceof SimpleName) {
			tmpSimpleName = ((SimpleName) node.getOperand());
			if (tmpSimpleName.resolveBinding() instanceof IVariableBinding)
				addCount((IVariableBinding) tmpSimpleName.resolveBinding(), node);
		}
		return false;
	}

	@Override
	public boolean visit(CompilationUnit node) {
		methodToVariables = new HashMap<IMethodBinding, List<IVariableBinding>>();// might
																					// want
																					// to
																					// override
																					// the
																					// hash
																					// function
		methodStringToVariables = new HashMap<>();
		variablesToAssignments = new HashMap<IVariableBinding, List<Expression>>();// might
																					// want
																					// to
																					// override
																					// the
																					// hash
																					// function
		tempStringsToRefactor = new ArrayList<String>();
		tempPositionsToRefactor = new ArrayList<>();
		FieldListerVisitor FLVisitor = new FieldListerVisitor();
		node.accept(FLVisitor);
		fieldVariables = FLVisitor.getFieldBindings();
		return super.visit(node);
	}

	@Override
	public boolean visit(VariableDeclarationStatement node) {
		for (Object o : node.fragments()) {
			VariableDeclarationFragment f = (VariableDeclarationFragment) o;
			IVariableBinding varBinding = f.resolveBinding();
			if (currentMethodBinding != null)
				methodToVariables.get(currentMethodBinding).add(varBinding);
			else
				methodStringToVariables.get(currentMethodString).add(varBinding);
			if (f.getInitializer() != null) {
				inInitialization = true;
				addCount(varBinding, f.getInitializer());
				inInitialization = false;
			}
		}

		return false;
	}

	@Override
	public boolean visit(Assignment node) {
		// check that is simple variable
		if (!(node.getLeftHandSide() instanceof SimpleName))
			return false;

		IBinding tempBiding = ((SimpleName) node.getLeftHandSide()).resolveBinding();
		if (!(tempBiding instanceof IVariableBinding))
			return false;
		IVariableBinding varBinding = (IVariableBinding) tempBiding;
		// verify that it is not a field
		if (fieldVariables.contains(varBinding))
			return false;
		addCount(varBinding, node.getRightHandSide());
		return false;
	}

	@Override
	public boolean visit(IfStatement node) {
		if (rootIfStmt == null)
			rootIfStmt = node;
		if (currIfStmt != null) {
			if (inIfBlock) {
				if (!childIfStmtOfThenIfStmt.containsKey(currIfStmt))
					childIfStmtOfThenIfStmt.put(currIfStmt, new ArrayList<>());
				childIfStmtOfThenIfStmt.get(currIfStmt).add(node);
			} else {
				if (!childIfStmtOfElseIfStmt.containsKey(currIfStmt))
					childIfStmtOfElseIfStmt.put(currIfStmt, new ArrayList<>());
				childIfStmtOfElseIfStmt.get(currIfStmt).add(node);
			}
		}
		currIfStmt = node;
		inIfBlock = true;
		thenBlockAssignment.put(currIfStmt, new HashMap<>());
		elseBlockAssignment.put(currIfStmt, new HashMap<>());
		node.getThenStatement().accept(this);
		inIfBlock = false;
		if (node.getElseStatement() != null) {
			inElseBlock = true;
			currIfStmt = node;
			elseBlockAssignment.put(currIfStmt, new HashMap<>());
			node.getElseStatement().accept(this);
			inElseBlock = false;
		}
		currIfStmt = null;
		if (currIfStmt == rootIfStmt)
			rootIfStmt = null;
		return false;
	}

	@Override
	public boolean visit(Block node) {
		for (Object o : node.statements()) {
			ASTNode s = (ASTNode) o;
			if (!(s instanceof IfStatement))
				s.accept(this);
		}
		for (Object o : node.statements()) {
			ASTNode s = (ASTNode) o;
			if (s instanceof IfStatement)
				s.accept(this);
		}
		return false;
	}
	protected void setNames(MethodDeclaration node)
	{
		currentMethodBinding = node.resolveBinding();
		String className = node.getParent() instanceof AbstractTypeDeclaration ? 
				((AbstractTypeDeclaration) node.getParent()).getName().getFullyQualifiedName():"AnonymousClass";
		currentMethodString =  className+ "." + node.getName().toString();
	}
}
