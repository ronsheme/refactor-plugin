package utils.visitors;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.Assignment;
import org.eclipse.jdt.core.dom.ClassInstanceCreation;
import org.eclipse.jdt.core.dom.IBinding;
import org.eclipse.jdt.core.dom.IVariableBinding;
import org.eclipse.jdt.core.dom.PostfixExpression;
import org.eclipse.jdt.core.dom.PrefixExpression;
import org.eclipse.jdt.core.dom.SimpleName;
import org.eclipse.jdt.core.dom.VariableDeclarationFragment;

/**
 * 
 * @author Ron Shemer ChangedVariableListerVisitor should start from a statement
 *         or an expression and recursively mark the variables (SimpleName) that
 *         might be (or is) affected by executing/evaluating the root
 *         statement/expression
 *
 */
public class ChangedVariableListerVisitor extends ASTVisitor {

	private List<IVariableBinding> changedVariables = new ArrayList<>();
	boolean inSideEffect = false;

	public List<IVariableBinding> getChangedVariables() {
		return changedVariables;
	}

	/*
	 * @Override public boolean visit(ClassInstanceCreation node) { inSideEffect
	 * = true; if(node.getExpression() != null)
	 * node.getExpression().accept(this); if(node.arguments()!=null) for(Object
	 * o:node.arguments()) ((ASTNode) o).accept(this);
	 * if(node.getAnonymousClassDeclaration()!=null)
	 * node.getAnonymousClassDeclaration().accept(this); inSideEffect = false;
	 * return false;
	 * 
	 * }
	 */
	
	@Override
	public boolean visit(VariableDeclarationFragment node) {
		if (!addLHSToChangedList(node))
			return false;
		if (node.getInitializer() != null)
			node.getInitializer().accept(this);
		return false;
	}

	@Override
	public boolean visit(Assignment node) {
		if (!addLHSToChangedList(node))
			return false;
		if (node.getRightHandSide() != null)
			node.getRightHandSide().accept(this);
		return false;
	}

	private boolean addLHSToChangedList(Assignment node) {
		if (!(node.getLeftHandSide() instanceof SimpleName))
			return false;
		IBinding tempBiding = ((SimpleName) node.getLeftHandSide()).resolveBinding();
		if (!(tempBiding instanceof IVariableBinding))
			return false;
		changedVariables.add((IVariableBinding) tempBiding);
		return true;
	}

	private boolean addLHSToChangedList(VariableDeclarationFragment node) {
		if (node.getInitializer() == null)
			return false;
		// check that is simple variable
		if (!(node.getName() instanceof SimpleName))
			return false;
		IBinding tempBiding = ((SimpleName) node.getName()).resolveBinding();
		if (!(tempBiding instanceof IVariableBinding))
			return false;
		changedVariables.add((IVariableBinding) tempBiding);
		return true;
	}

	/**
	 * visit(PrefixExpression node): there are some prefixes that are not
	 * causing side-effect so we look only for ++/--, variables in the operand
	 * are marked as changed
	 */
	@Override
	public boolean visit(PrefixExpression node) {
		if (!(node.getOperator() == PrefixExpression.Operator.DECREMENT
				|| node.getOperator() == PrefixExpression.Operator.INCREMENT))
			return super.visit(node);
		inSideEffect = true;
		node.getOperand().accept(this);
		inSideEffect = false;
		return false;
	}

	/**
	 * visit(PostfixExpression node): in postfix expressions only ++/-- are
	 * allowed, which means side-effect always
	 */
	@Override
	public boolean visit(PostfixExpression node) {
		inSideEffect = true;
		node.getOperand().accept(this);
		inSideEffect = false;
		return false;
	}

	/**
	 * visit(SimpleName node): should be a variable, either local or a field. If
	 * it is a primitive and not in a side-effect expression then it is ignored
	 * since the value is not changed, only used. It is necessarily in LHS
	 * according to the way visit works with Assignment and
	 * VariableDeclerationFragment
	 */
	@Override
	public boolean visit(SimpleName node) {
		Object tempBinding = node.resolveBinding();
		if (!(tempBinding instanceof IVariableBinding))
			return false;
		if (inSideEffect || !((IVariableBinding) tempBinding).getType().isPrimitive())
			changedVariables.add((IVariableBinding) tempBinding);
		return false;
	}
}
