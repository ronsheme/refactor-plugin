package utils.visitors;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.Assignment;
import org.eclipse.jdt.core.dom.Assignment.Operator;
import org.eclipse.jdt.core.dom.Block;
import org.eclipse.jdt.core.dom.ForStatement;
import org.eclipse.jdt.core.dom.IVariableBinding;
import org.eclipse.jdt.core.dom.IfStatement;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.SimpleName;
import org.eclipse.jdt.core.dom.Statement;
import org.eclipse.jdt.core.dom.VariableDeclarationFragment;
import org.eclipse.jdt.core.dom.WhileStatement;
import org.jgrapht.DirectedGraph;
import org.jgrapht.Graph;
import org.jgrapht.graph.DefaultDirectedGraph;
import org.jgrapht.graph.DefaultEdge;

import utils.GraphsUtils;

/**
 * 
 * @author Ron Shemer in a sense, flow-sensitive is actually flow-insensitive
 *         with additional removeOldEdges action before any edge addition, and
 *         special treatment for conditionals and loops
 *
 */
public class LocalVariableFlowSensitiveDependencyGraphVisitor extends FlowInsensitiveDirectedDependencyGraphVisitor {

	Statement stmt;// marks where to stop the graph evaluation
	boolean visitElseClause = false;
	boolean inLoop = false;
	boolean inThen = false;
	boolean inElse = false;
	Graph<IVariableBinding, DefaultEdge> tempElseGraph = null;
	Graph<IVariableBinding, DefaultEdge> tempThenGraph = null;

	public LocalVariableFlowSensitiveDependencyGraphVisitor(Statement stmt, boolean visitElseClause) {
		this.stmt = stmt;
		this.visitElseClause = visitElseClause;
	}

	@Override
	public boolean visit(ForStatement node) {
		a=0;
		inLoop = true;
		super.visit(node);
		inLoop = false;
		return false;
	}

	@Override
	public boolean visit(WhileStatement node) {
		inLoop = true;
		super.visit(node);
		inLoop = false;
		return false;
	}

	@Override
	public boolean visit(Block node) {
		for (Object stmt : node.statements()) {
			if (stmt == this.stmt)
				return false;
			((ASTNode) stmt).accept(this);
		}
		return super.visit(node);
	}

	@Override
	public boolean visit(Assignment node) {
		if (!setLHSBinding(node))
			return false;
		if (!(inThen || inElse)) {
			if (node.getOperator() == Operator.ASSIGN)
				removeOldEdges();
			return super.visit(node);
		}
		if (inThen) {
			if (node.getOperator() == Operator.ASSIGN)
				removeOldEdges(tempThenGraph);
		} else {
			if (node.getOperator() == Operator.ASSIGN)
				removeOldEdges(tempElseGraph);
		}
		node.getRightHandSide().accept(this);
		currLHS = null;
		return false;
	}

	@Override
	public boolean visit(VariableDeclarationFragment node) {
		if (!setLHSBinding(node))
			return false;
		if (!(inThen || inElse)) {
			removeOldEdges();
			return super.visit(node);
		}
		node.getInitializer().accept(this);
		currLHS = null;
		return false;
	}

	@Override
	public boolean visit(SimpleName node) {
		if (!(node.resolveBinding() instanceof IVariableBinding) || currLHS == null)
			return false;
		if (((IVariableBinding) node.resolveBinding()).getType().isPrimitive())
			return false;
		if (!(inThen || inElse))
			return super.visit(node);
		IVariableBinding currVar = (IVariableBinding) node.resolveBinding();
		if (inThen)
			GraphsUtils.safeAddEdge(tempThenGraph,currLHS,currVar);
		else 
			GraphsUtils.safeAddEdge(tempElseGraph, currLHS, currVar);
		return false;
	}

	@Override
	public boolean visit(IfStatement node) {
		if(node.getElseStatement() == null)
			return super.visit(node);
		
		boolean flag = false;
		flag = node.getThenStatement() == this.stmt;
		flag = flag || node.getElseStatement() == this.stmt;
		if (node.getThenStatement() instanceof Block)
			for (Object stmt : ((Block) node.getThenStatement()).statements())
				flag = flag || stmt == this.stmt;
		if (node.getElseStatement() instanceof Block)
			for (Object stmt : ((Block) node.getElseStatement()).statements())
				flag = flag || stmt == this.stmt;
		
		if (!flag) {
			inThen = true;
			tempThenGraph = new DefaultDirectedGraph<IVariableBinding, DefaultEdge>(DefaultEdge.class);
			copyGraph(g, tempThenGraph);
			node.getThenStatement().accept(this);
			inThen = false;

			inElse = true;
			tempElseGraph = new DefaultDirectedGraph<IVariableBinding, DefaultEdge>(DefaultEdge.class);
			copyGraph(g, tempElseGraph);
			node.getElseStatement().accept(this);
			inElse = false;
			unionGraphsIntoG(tempThenGraph, tempElseGraph, g);
			return false;
		}
		if (!visitElseClause || node.getElseStatement() != null) {
			node.getThenStatement().accept(this);
			// no need to consider the other clause because when we want to push
			// down or pull up
			// we only care for dependency below or above within the clause
			return false;
		}
		node.getElseStatement().accept(this);
		return false;
	}

	private void unionGraphsIntoG(Graph<IVariableBinding, DefaultEdge> tempGraph1,
			Graph<IVariableBinding, DefaultEdge> tempGraph2, Graph<IVariableBinding, DefaultEdge> g) {
		for (IVariableBinding v : tempGraph1.vertexSet())
			if (!g.containsVertex(v))
				g.addVertex(v);
		for (IVariableBinding v : tempGraph2.vertexSet())
			if (!g.containsVertex(v))
				g.addVertex(v);
		for (IVariableBinding v1 : g.vertexSet())
			for (IVariableBinding v2 : g.vertexSet()) {
				if (tempGraph1.containsEdge(v1, v2) || tempGraph2.containsEdge(v1, v2))
					g.addEdge(v1, v2);
			}

	}

	private void copyGraph(Graph<IVariableBinding, DefaultEdge> g, Graph<IVariableBinding, DefaultEdge> tempGraph) {
		for (IVariableBinding v : g.vertexSet())
			tempGraph.addVertex(v);
		for (IVariableBinding v1 : g.vertexSet())// because shitty DefaultEdge
													// protects getSource()
													// ,getTarget()
			for (IVariableBinding v2 : g.vertexSet())
				if (g.containsEdge(v1, v2))
					tempGraph.addEdge(v1, v2);
	}

	@Override
	public boolean visit(MethodDeclaration node) {
		Block stmts = node.getBody();
		for (Iterator iterator = stmts.statements().iterator(); iterator.hasNext();) {
			Statement stmt = (Statement) iterator.next();
			if (stmt == this.stmt)
				return false;
			stmt.accept(this);
		}
		return false;
	}

	private void removeOldEdges() {
		removeOldEdges(g);
	}

	private void removeOldEdges(Graph<IVariableBinding, DefaultEdge> depG) {
		if (inLoop)
			return;
		if (!depG.containsVertex(currLHS))
			return;
		Set<DefaultEdge> edges = new HashSet<DefaultEdge>();
		edges.addAll(((DirectedGraph) depG).outgoingEdgesOf(currLHS));
		depG.removeAllEdges(edges);
	}
}
