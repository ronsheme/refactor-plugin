package utils.visitors;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.Assignment;
import org.eclipse.jdt.core.dom.IVariableBinding;
import org.eclipse.jdt.core.dom.SimpleName;
import org.eclipse.jdt.core.dom.TypeDeclaration;
import org.eclipse.jdt.core.dom.VariableDeclarationFragment;
import org.eclipse.jdt.core.dom.Assignment.Operator;

/**
 * 
 * @author Ron Shemer DependentVariableListerVisitor lists all the variables
 *         appears in a statement
 *
 */
public class DependentVariableListerVisitor extends ASTVisitor {

	List<IVariableBinding> dependentVariables = new ArrayList<>();

	public List<IVariableBinding> getDependentVariables() {
		return dependentVariables;
	}
	@Override
	public boolean visit(Assignment node) {
		return super.visit(node);
	}
	@Override
	public boolean visit(VariableDeclarationFragment node) {
		if(node.getInitializer() != null)
			node.getInitializer().accept(this);
		return false;
	}
	@Override
	public boolean visit(SimpleName node) {
		Object tempBinding = node.resolveBinding();
		if (!(tempBinding instanceof IVariableBinding))
			return false;
		dependentVariables.add((IVariableBinding) tempBinding);
		return false;
	}
}
