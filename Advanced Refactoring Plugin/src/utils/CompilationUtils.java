package utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.dom.AST;
import org.eclipse.jdt.core.dom.ASTParser;
import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.apache.commons.lang3.SystemUtils;

/**
 * 
 * @author Ron Shemer CompilationUtils contains methods related to compilation
 *         of java programs, including compiling from source path and outputting
 *         AST and traversing it
 */
public class CompilationUtils {

	private static final String PATH_TO_RT_JAR_WIN = "C:\\Program Files\\Java\\jre1.8.0_60\\lib\\rt.jar";
	private static final String PATH_TO_RT_JAR_LINUX = "/usr/lib/jvm/java-8-oracle/jre/lib/rt.jar";

	/**
	 * 
	 * @param path-
	 *            to source file
	 * @return source file as char array
	 * @throws IOException-
	 *             might throw if the file in path is not readable
	 */
	static char[] getCharArrayFromFile(String path) throws IOException {
		File sourceFile = new File(path);
		FileInputStream fis = new FileInputStream(sourceFile);
		byte[] data = new byte[(int) sourceFile.length()];
		fis.read(data);
		fis.close();
		return new String(data, "UTF-8").toCharArray();
	}

	/**
	 * 
	 * @param unitName
	 * @param source
	 * @param sourcePath
	 * @param visitor
	 * 
	 *            compiles the source, retrieve the AST and run the specified
	 *            visitor
	 */
	public static void runRefactor(CompilationPaths compPaths, ASTVisitor visitor) {
		CompilationUnit ast = getCompilationUnit(compPaths);

		// ast.accept(new PrinterVisitor());
		ast.accept(visitor);
	}

	/**
	 * 
	 * @param compPaths
	 * @return compiled source as AST
	 */
	public static CompilationUnit getCompilationUnit(CompilationPaths compPaths) {
		ASTParser parser = ASTParser.newParser(AST.JLS8);

		parser.setResolveBindings(true);
		parser.setKind(ASTParser.K_COMPILATION_UNIT);
		parser.setProject(compPaths.javaProject);
		parser.setUnitName("");

		String[] sources = { new File(compPaths.sourcePath).getParent() };
		String[] classpath = { SystemUtils.IS_OS_LINUX ? PATH_TO_RT_JAR_LINUX : PATH_TO_RT_JAR_WIN };

		parser.setEnvironment(classpath, sources, new String[] { "UTF-8" }, true);
		char[] sourceAsArray = null;
		try {
			sourceAsArray = compPaths.source ==null?
				getCharArrayFromFile(compPaths.sourcePath):compPaths.source.toCharArray();
		} catch (IOException e) {
			e.printStackTrace();
		}
		parser.setSource(sourceAsArray);// getCharArrayFromFile(args[0]));

		CompilationUnit ast = (CompilationUnit) parser.createAST(null);
		return ast;
	}
	
	public static String getPackageNameFromFilePath(CompilationPaths compPaths)
	{
		CompilationUnit ast = getCompilationUnit(compPaths);
		String ret = ast.getPackage() == null? "" : ast.getPackage().getName().getFullyQualifiedName();
		return ret;
	}
}
