package utils;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class FileToString {

	/**
	 * Extract the content of a file into string.
	 * 
	 * @param file - the path to the file. 
	 * @return the content of the file as String object.
	 * @throws FileNotFoundException - in case the system was not able to find the specified path. 
	 */
	public static String fileToString(File file) throws FileNotFoundException {
		if(file.length() == 0)
			return "";
		
		Scanner s = new Scanner(file);
		String fileContent = s.useDelimiter("\\Z").next();
		s.close();
		return fileContent;
	}
}
