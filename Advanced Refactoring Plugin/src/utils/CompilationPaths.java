package utils;

import org.eclipse.jdt.core.IJavaProject;

public class CompilationPaths {
	public String unitName;
	public String source;
	public String sourcePath;
	public IJavaProject javaProject;

	public CompilationPaths(String unitName, String source, String sourcePath, IJavaProject javaProject) {
		this.unitName = unitName;
		this.source = source;
		this.sourcePath = sourcePath;
		this.javaProject = javaProject;
	}
}