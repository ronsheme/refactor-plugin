package utils;

import java.io.File;
import java.util.LinkedList;
import java.util.List;

/**
 * This auxiliary class contains one public function that collects
 * all xxx.java files from a given Java project. 
 */
public class JavaProjectFileCollector {
	
	/* Path constants */
	//private static final String srcFolderString = "C:/Ron/git/refactor-plugin/Advanced Refactoring Plugin/src";
	private static final String javaFileSuffix = ".java";

	/**
	 * returns a list of all .java file names in the project
	 * @return a list of all .java file names in the project.
	 */
	public static List<String> getJavaFiles(String srcFolderString) {
		List<String> javaFiles = new LinkedList<String>();
		File src = new File(srcFolderString);
		String[] fileNames = src.list();
		StringBuilder packagesPath = new StringBuilder();
		return getJavaFilesRec(javaFiles, fileNames, packagesPath,srcFolderString);	
	}

	/* Recursively scan project's packages */
	private static List<String> getJavaFilesRec(List<String> javaFiles,
			String[] fileNames, StringBuilder packagesPath, String srcFolderString) {
		
		for (int i = 0; i < fileNames.length; i++) {
			String currFile = fileNames[i];
			
			if(currFile.endsWith(javaFileSuffix)) {
				String formatedClassPath = classPathFormat(packagesPath, currFile);
				javaFiles.add(formatedClassPath);
				continue;
			}
			
			File currFilePath = new File(srcFolderString + packagesPath + "/" + currFile);
			String record = packagesPath.toString();
			if(currFilePath.isDirectory()) {
				packagesPath.append("/" + currFile);
				getJavaFilesRec(javaFiles, currFilePath.list(), packagesPath,srcFolderString);
				packagesPath = new StringBuilder(record);
			}
		}
		return javaFiles;	
	}
	
	/* Auxiliary function that forms the class path */
	private static String classPathFormat(StringBuilder sb, String s) {
		String temp = sb.substring(1) + "." + s;
		String temp1 = temp.substring(0, temp.length()-5);
		String temp2 = temp1.replace('/', '.');
		return temp2;
	}
}
