package utils;

public class Index {
	private int index;
	private int length;
	
	public Index(int index, int length) {
		this.index = index;
		this.length = length;
	}
	public int getIndex() {
		return index;
	}
	public int getIndexEnd() {
		return index+length;
	}
	public void setIndex(int index) {
		this.index = index;
	}
	public int getLength() {
		return length;
	}
	public void setLength(int length) {
		this.length = length;
	}
}
