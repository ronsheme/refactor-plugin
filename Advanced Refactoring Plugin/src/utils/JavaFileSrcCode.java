package utils;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import org.apache.commons.io.FileUtils;

public class JavaFileSrcCode {

	private static final int binStringLength = 4;
	private static final int slashStringLength = 1;
	private static final String srcFolder = "src/";
	private static final String srcSuffix = ".java";
	
	public static String getJavaFileSrcCode(Class<?> c) throws URISyntaxException, IOException  {
		
		if(c == null) {
			System.out.println("Cannot get source code null object. null is returned.");
			return null;
		}
		
		String srcFilePath = getSrcFilePath(c);
		String destDirPath = getDestinationDirPath(c);
		copyFile(srcFilePath, destDirPath);
		
		File srcFileInBin = getCopiedFileFromBin(c);
		String srcCode = FileToString.fileToString(srcFileInBin);
		
		FileUtils.forceDelete(srcFileInBin);
		
		return srcCode;
	}
	
	private static String getDestinationDirPath(Class<?> c) {
		String urlString = getResourceAsString(c);
		String destDirPath = urlString.substring(0, urlString.length()-slashStringLength);
		return destDirPath;
	}
	
	private static String getSrcFilePath(Class<?> c) {
		String urlString = getResourceAsString(c);
		String projectDirPath = urlString.substring(0, urlString.length()-binStringLength);
		String classPath = c.getName().replace('.', '/');
		String fullPath = projectDirPath + srcFolder + classPath + srcSuffix;
		return fullPath;
	}
	
	private static File getCopiedFileFromBin(Class<?> c) throws URISyntaxException {
		String simpleName = c.getSimpleName() + srcSuffix;
		URL copiedFileUrl = c.getResource(simpleName);
		File srcFileInBin = new File(copiedFileUrl.toURI());
		return srcFileInBin;
	}
	
	private static String getResourceAsString(Class<?> c) {
		URL url = c.getResource("");
		return url.toString();
	}
	
	private static void copyFile(String srcFilePath, String destDirPath) throws IOException {
		File srcFile = new File(URI.create(srcFilePath));
		File destDir = new File(URI.create(destDirPath));
		FileUtils.copyFileToDirectory(srcFile, destDir);
	}
}
