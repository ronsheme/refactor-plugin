package utils;

/**
 * 
 * @author Ron IndexPair class holds two ranges: [index1 ,(index1+index1end)]
 *         and [index2 ,(index2+index2end)] intended to keep the location of a
 *         pair of statements that can be consolidated.
 */
public class IndexPair {
	private Index index1;
	private Index index2;

	public IndexPair(int index1, int index2, int index1length, int index2length) {
		this.index1 = new Index(index1, index1length);
		this.index2 = new Index(index2, index2length);
	}

	public int getIndex1start() {
		return index1.getIndex();
	}

	public int getIndex1end() {
		return index1.getIndex() + index1.getLength();
	}

	public int getIndex2start() {
		return index2.getIndex();
	}

	public int getIndex2end() {
		return index2.getIndex()+ index2.getLength();
	}

	@Override
	public boolean equals(Object o) {
		if (o == null || !(o instanceof IndexPair))
			return false;
		// enough to verify only this because the begining of a statement in
		// unique to it
		return ((IndexPair) o).index1.getIndex() == this.index1.getIndex()
				&& ((IndexPair) o).index2.getIndex() == this.index2.getIndex();
	}
}
