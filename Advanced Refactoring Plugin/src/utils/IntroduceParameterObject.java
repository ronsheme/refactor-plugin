package utils;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.commons.lang3.builder.HashCodeBuilder;

/**
 * Often a particular group of parameters tends to be passed together. Several
 * methods may use this group. Such a group of classes is a data clump and can 
 * be replaced with an object that carries all of this data. This refactoring is 
 * useful because it reduces the size of the parameter lists, and long parameter
 * lists are hard to understand and less consistent.
 * The algorithm implemented in this class finds cases in which a multiset of 
 * parameter types is passed  frequently to methods. It is suggested to introduce
 * a new object type that includes all the types in the set as fields. The new 
 * object type will be passed instead of the multiset.
*/
public class IntroduceParameterObject {

	/* Contains all class methods & constructors */
	private Object[] classMethods;
		
	/* Methods & string parameters table */
	private Map<Object, String[]> methodStringParameterTable;		
	
	/* Auxiliary static data structure */
	private static List<String[]> result = new LinkedList<String[]>();
	
	/* 
	 * This constant limits the number of types that the analysis
	 * is able to recommend for clustering. It does so in order to
	 * prevent a potential time complexity blowup. The algorithm
	 * relies on finding all the subsets of size k,k-1,...,2
	 * which is known to be hard. Of course, it is possible to 
	 * increase/decrease the size of this constant correspondingly  
	 * with the classes we run the analysis on, availability of time
	 * and system's computational power. Running time is in straight
	 * correlation with the number of methods in class, and the 
	 * number of formal parameters in each method.
	 */
	private static final int subsetSizeLimitation = 5;
	
	/**
	 * Constructs an IntroduceParameterObject analyzer on a given class.
	 * @param c Class<?> object that represents the class to run the
	 * analysis on. 
	 */
	public IntroduceParameterObject(Class<?> c) {
		if (c == null) {
			System.out.println("Error: cannot run analysis on a null class."
					+ " Please try again.");
			return;
		}
		/* Both methods and constructors are taken into account for the analysis */
		Constructor<?>[] constructors = c.getConstructors();
		Method[] methods = c.getDeclaredMethods();
		classMethods = methodsAndConstructors(constructors, methods);
		methodStringParameterTable = new HashMap<Object, String[]>();

		/* Build class method-parameters tables */
		for (int i = 0; i < classMethods.length; i++) {
			Object m = classMethods[i];
			methodStringParameterTable.put(m, getStringParameterTypes(m));
		}
	}
	
	/*
	 * Helper for constructor. Our analysis takes into account both methods
	 * and constructors. This function creates an array of all class methods
	 * and constructors.
	 */
	private Object[] methodsAndConstructors(Constructor<?>[] c, Method[] m) {
		Object[] allMethods = new Object[c.length + m.length];
		for (int i = 0; i < c.length; i++) {
			allMethods[i] = c[i];
		}
		int i = 0;
		for (int j = c.length; j < allMethods.length; j++) {
			allMethods[j] = m[i];
			i++;
		}
		return allMethods;
	}
	
	/*
	 * Helper for constructor. For each method constructs an array of strings,
	 * each string is a representation of a parameter type (including generic
	 * types).
	 */
	private String[] getStringParameterTypes(Object o) {
		/* Handle Method objects */
		String[] paramsAsStrings = null;
		if (o instanceof Method) {
			Parameter[] params = ((Method) o).getParameters();
			int numOfParams = params.length;
			paramsAsStrings = new String[numOfParams];
			for (int i = 0; i < numOfParams; i++) {
				paramsAsStrings[i] = params[i].getParameterizedType().getTypeName();
			}
		/* Handle Constructor objects */
		} else {
			Type[] params = ((Constructor<?>) o).getGenericParameterTypes();
			int numOfParams = params.length;
			paramsAsStrings = new String[numOfParams];
			for (int i = 0; i < numOfParams; i++) {
				paramsAsStrings[i] = params[i].getTypeName();
			}
		}
		return paramsAsStrings;
	}
	
	/**
	 * Run the Introduce Parameter Object analysis.
	 * The result will be presented as a table: on the left column are the
	 * multiset of parameter types to be clustered, and on the right are 
	 * the methods that their parameter type list contain this multiset. 
	 * Prints an informative report to the console also.
	 * 
	 * @return the resulting table described above.
	 */
	public Map<String[], Set<Object>> runClassAnalysis() {
		/* Handle cases where the analysis is irrelevant */
		if (classMethods.length < 2) {
			System.out.println("Introduce Parameter Object analysis is"
					+ " irrelevant for classes with less than two methods.");
			return null;
		}
		int max = maxNumOfMethodParameters();
		if (max < 2) {
			System.out.println("Introduce Parameter Object analysis is irrelevant for"
					+ " classes in which all methods contain 0 or 1 parameters.");
			return null;
		}
		
		/* Assign value for k (5 or smaller) */
		int k = (max > subsetSizeLimitation ? subsetSizeLimitation : max);
		/* Holds cluster analysis results */
		Map<String[], Set<Object>> result = new HashMap<String[], Set<Object>>();

		/* Loop through k size subsets, gradually decrease k */
		while (k >= 2) {
			Set<SubsetRecord> possibleSubsets = allPossibleParameterK_Subsets(k);
			SubsetRecord maximalSubset = maxSubset(possibleSubsets);
			/*
			 *  Impossible to find a k size subset to cluster, try to find k-1 size
			 *  such subset. 
			 */
			if (!clusterPossible(maximalSubset)) {
				k--;
				continue;
			}
			/* 
			 * Analysis has located a k size subset of types to cluster.
			 * The types in this subset are removed from the the relevant
			 * parameters lists (the ones that contain the subset). 
			 * Continue to search for subsets to cluster.
			 */
			String[] maxSubset = maximalSubset.subset;
			Map<Object, String[]> reducedParameters = reduceParameters(maxSubset);
			methodStringParameterTable = reducedParameters;
			result.put(maxSubset, maximalSubset.appearsInSet);
		}
		printAnalysisReport(result);
		return result;
	}
	
	/* 
	 * Print analysis report.
	 */
	private void printAnalysisReport(Map<String[], Set<Object>> table) {
		if(table.isEmpty()) {
			System.out.println("Class analysis has terminated without"
					+ " any clustering recommendations.");
			return;
		}
		for (String[] params : table.keySet()) {
			Set<Object> inMethods = table.get(params);
			System.out.println("The following list of parameter types may be"
					+ " clustered as a new Java object:");
			System.out.println(Arrays.toString(params));
			System.out.println("These parameters appear as sublist of the"
					+ " parameter list in the following methods:");
			for (Object method : inMethods) {
				if(method instanceof Method)
					System.out.println(((Method) method).getName());
				if(method instanceof Constructor<?>)
					System.out.println(((Constructor<?>) method).getName());
			}
			System.out.println();
		}
	}
	
	/*
	 * Returns the maximal number of parameters that a method in 
	 * this class receives.
	 */
	public int maxNumOfMethodParameters() {
	int max = 0;
	
	for (String[] paramList : this.methodStringParameterTable.values()) {
		int len = paramList.length;
		if(len > max)
			max = len; 
	}
	return max;
}

	/*
	 * The following two methods implement the k-size sublist algorithm.
	 * It receives k, and an array of strings. The array contains the 
	 * string representation of method parameter types. It returns all
	 * k-size sublists of the given list of types. 
	 */
	private static List<String[]> processParametersSubsets(String[] set, int k) {
		String[] subset = new String[k];
	    processLargerParameterSubsets(set, subset, 0, 0);
	    List<String[]> copy = result;
	    result = new LinkedList<String[]>();
	    return copy;
	}

	private static void processLargerParameterSubsets(String[] set, String[] subset,
			int subsetSize, int nextIndex) {
	    if (subsetSize == subset.length) {
	    	String[] copy = new String[subset.length];
	    	for (int i = 0; i < copy.length; i++) {
	    		copy[i] = subset[i];
			}
	    	result.add(copy);
	    } else {
	        for (int j = nextIndex; j < set.length; j++) {
	            subset[subsetSize] = set[j];
	            processLargerParameterSubsets(set, subset, subsetSize + 1, j + 1);
	        }
	    }
	}
	
	/*
	 * Create and return a method-sublists table. Namely, for each method
	 * in class, match all it's k-size sublists of parameter types.
	 */
	private Map<Object, List<String[]>> methodParameterSubsetsTable(int k) {
		Map<Object, List<String[]>> table = new HashMap<Object, List<String[]>>();
		
		for (Object method : methodStringParameterTable.keySet()) {
			String[] stringParams = methodStringParameterTable.get(method);
			table.put(method, processParametersSubsets(stringParams, k));
		}
		return table;
	}
	
	/*
	 * The goal of this method is to collect all possible sublists of
	 * of parameter types into one set. For each sublist we initialize
	 * a SubsetRecord object which is a nested class of this class.
	 */
	private Set<SubsetRecord> allPossibleParameterK_Subsets(int k) {
		Map<Object, List<String[]>> table = methodParameterSubsetsTable(k);		
		List<String[]> subsets = new LinkedList<String[]>();
		
		for (List<String[]> value : table.values()) {
			for (String[] strings : value) {
				subsets.add(strings);
			}
		}
		Set<SubsetRecord> subsetRecords = new HashSet<SubsetRecord>();
		for (String[] subset : subsets) {
			Set<Object> appearsInSet = appearsInMethods(subset);
			int numOfAppearances = appearsInSet.size();
			SubsetRecord record = new SubsetRecord(subset, appearsInSet, numOfAppearances);
			subsetRecords.add(record);
		}
		return subsetRecords;	
	}
	
	/*
	 * Iterate over class methods, check if the given subset is contained
	 * in the parameter list of a method. Return the set of those methods.
	 */
	private Set<Object> appearsInMethods(String[] subset) {
		Set<Object> result = new HashSet<Object>();
		for (Object m : methodStringParameterTable.keySet()) {
			String[] stringParams = methodStringParameterTable.get(m);
			if(isSublist(subset, stringParams))
				result.add(m);
		}
		return result;	
	}
	
	/*
	 * According to the algorithm, at each stage we must find the
	 * largest list of types to cluster.
	 */
	private SubsetRecord maxSubset(Set<SubsetRecord> records) {
		SubsetRecord result = new SubsetRecord(new String[0], new HashSet<Object>(), 0);
		int max = 0;
		
		for (SubsetRecord subsetRecord : records) {
			if(subsetRecord.numOfAppearences > max) {
				max = subsetRecord.numOfAppearences;
				result = subsetRecord;
			}
		}
		return result;
	}
	
	/*
	 * Given two string arrays representing lists of parameter types,
	 * return true iff the first is a sublist of the second.
	 */
	private boolean isSublist(String[] subset, String[] params) {
		List<String> subList = new LinkedList<String>(Arrays.asList(subset));
		List<String> list = new LinkedList<String>(Arrays.asList(params));
		
		for (int i = 0; i < subList.size(); i++) {
			String stringParam = subList.get(i);
			if(!list.contains(stringParam))
				return false;
			else {
				list.remove(stringParam);
			}
		}
		return true;
	}	
	
	/*
	 * In order to cluster, the sublist of parameter types must be contained
	 * in the parameter list of at least two methods.
	 */
	private boolean clusterPossible(SubsetRecord subsetRecord) {
		return subsetRecord.numOfAppearences >= 2;
	}
	
	/*
	 * After we found a sublist of types to cluster, we remove the types from 
	 * the list of parameter types of the relevant methods.  
	 */
	private Map<Object, String[]> reduceParameters(String[] subset) {
		Map<Object, String[]> stringParamsTable = new HashMap<Object, String[]>();
		
		for (Object method : methodStringParameterTable.keySet()) {
			String[] stringParams = methodStringParameterTable.get(method);
			if(isSublist(subset, stringParams)) {
				stringParamsTable.put(method, reduceMethodParameters(subset, stringParams));
			} else {
				stringParamsTable.put(method, stringParams);
			}
		}
		return stringParamsTable;
	}
	
	private String[] reduceMethodParameters(String[] subset, String[] paramList) {
		List<String> subList = new LinkedList<String>(Arrays.asList(subset));
		List<String> list = new LinkedList<String>(Arrays.asList(paramList));
		list.removeAll(subList);
		/* Java's strange way to convert list to array */
		String[] arr = new String[0];
		return list.toArray(arr);
	}
		
	/*
	 * The goal of this nested class is keeping an important information
	 * about each of the sublists of parameter type. The information is
	 * essential for the implementation of the analysis. For example, 
	 * for each sublist we hold all methods which their parameter types 
	 * list contains the sublist. Those parameter types can hence be 
	 * removed from the list of parameter types of each method.   
	 */
	private class SubsetRecord {
		
		/* The parameter type subset */
		private String[] subset;
		
		/* Set of methods which this subset contained in their formal parameter list */
		private Set<Object> appearsInSet;
		
		/* The number of appearances in methods */
		private int numOfAppearences = 0;
		
		private SubsetRecord(String[] subset, Set<Object> appearsInSet, int numOfAppearences){
			this.subset = subset;
			this.appearsInSet = appearsInSet;
			this.numOfAppearences = numOfAppearences;
		}
		
		@Override
		public String toString() {
			String result = "Subset Array: " + Arrays.toString(subset) + "\n"
					+ "Number of subset appearences in method signatures: " 
					+ numOfAppearences;
			return result;
		}
		
		/* Using Apache's hash function */
		public int hashCode() {
			return new HashCodeBuilder(17, 31).append(subset).toHashCode();
		}
	    
		/* 
		 * An implementation of equals. Two subset records are equal if
		 * the subsets themselves are equal. The subsets are equal if the
		 * representing arrays contain the same types (not necessarily 
		 * at the same order).
		 */
		@Override
		public boolean equals(Object obj) {
			if (!(obj instanceof SubsetRecord)) {
				return false;
			}
			SubsetRecord subsetRecord = (SubsetRecord) obj;
			return arrayEquals(this.subset, subsetRecord.subset);			
		}
			
		private boolean arrayEquals(String[] self, String[] other) {
			List<String> selfList = new LinkedList<String>(Arrays.asList(self));
			List<String> otherList = new LinkedList<String>(Arrays.asList(other));
			
			if(self.length != other.length) 
				return false;
			
			for (int i = 0; i < selfList.size(); i++) {
				String stringParam = selfList.get(i);
				if(!otherList.contains(stringParam))
					return false;
				else {
					otherList.remove(stringParam);
				}
			}
			return otherList.isEmpty();
		}
	}
	
}

