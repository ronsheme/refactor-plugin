package utils;

import java.util.HashSet;
import java.util.Set;

import org.jgrapht.Graph;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.traverse.DepthFirstIterator;

public class GraphsUtils {

	/**
	 * 
	 * @param v1-start
	 *            vertex
	 * @param v2-destination
	 *            vertex
	 * @param g-directed
	 *            graph
	 * @return true iff v2 is reachable from v1
	 */
	public static boolean isReachable(Object v1, Object v2, Graph g) {
		DepthFirstIterator DFSIterator = new DepthFirstIterator(g, v1);
		Object currVertex;
		currVertex = DFSIterator.next();// first should be the start vertex, so
										// probably not null

		while (DFSIterator.hasNext()) {
			if (currVertex == v2)
				return true;
			currVertex = DFSIterator.next();
		}
		if (currVertex == v2)
			return true;
		return false;
	}

	/**
	 * 
	 * @param v-
	 *            statring vertex
	 * @param g-
	 *            directed or undirected graph
	 * @return a set of the vertices in the connected component that starts from
	 *         v undirected graph-> strongly connected component that containg v
	 *         directed graph-> vertices reachable from v
	 */
	public static Set getConnectedComponent(Object v, Graph g) {
		DepthFirstIterator DFSIterator = new DepthFirstIterator(g, v);
		Set connectedComponent = new HashSet();

		while (DFSIterator.hasNext())
			connectedComponent.add(DFSIterator.next());
		return connectedComponent;
	}
	
	/**
	 * 
	 * @param g- graph
	 * @param v1- source
	 * @param v2- target
	 * 
	 * if v1 or v2 not vertices in g- add them
	 * if (v1,v2) not already an edge in g- add it
	 */
	public static void safeAddEdge(Graph g, Object v1, Object v2)
	{
		if (!g.containsVertex(v1))
			g.addVertex(v1);
		if (!g.containsVertex(v2))
			g.addVertex(v2);
		if (!g.containsEdge(v1, v2))
			g.addEdge(v1, v2);
		
	}
}
