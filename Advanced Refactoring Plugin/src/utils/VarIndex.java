package utils;

public class VarIndex extends Index {
	private String varName;
	
	public VarIndex(int index, int length,String varName) {
		super(index, length);
		this.varName=varName;
	}

	public String getVarName() {
		return varName;
	}
}
