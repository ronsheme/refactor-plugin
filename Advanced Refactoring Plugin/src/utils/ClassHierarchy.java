package utils;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * 
 * 
 */
public class ClassHierarchy {
		
	public static List<Class<?>> getBottomUpClassHierarchy(Class<?> c) {
		
		if(c == null) {
			System.out.println("Cannot find class hierarchy for null object. null is returned.");
			return null;
		}
		
		List<Class<?>> lst = new LinkedList<Class<?>>();
		while(c != null) {
			lst.add(c);
			Class<?> s = c.getSuperclass();
			c = s;			
		}
		return lst;	
	}
	
	public static List<Class<?>> getTopDownClassHierarchy(Class<?> c) {
		List<Class<?>> lst = getBottomUpClassHierarchy(c);
		Collections.reverse(lst);
		return lst;
	}
}
