package advanced_refactoring_plugin;

import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;

public class MarkerUtils {
	public static void deleteMarkers(IResource target, String type) {
		try {
			IMarker[] markers = target.findMarkers(type, false, IResource.DEPTH_INFINITE);
			for (int i = 0; i < markers.length; i++) {
				markers[i].delete();
			}
		} catch (CoreException e) {
			e.printStackTrace();
		}
	}

	public static void createReplaceTempWithQueryMarker(IResource resource, int startIndex, int endIndex) {
		try {
			IMarker marker = resource.createMarker("reafactoring.replacetempwitheuqrymarker");
			marker.setAttribute(IMarker.SEVERITY, IMarker.SEVERITY_WARNING);
			marker.setAttribute(IMarker.MESSAGE, "Consider replacing this temporary with a query method");
			marker.setAttribute(IMarker.CHAR_START, startIndex);
			marker.setAttribute(IMarker.CHAR_END, endIndex);

		} catch (CoreException e) {
			e.printStackTrace();
		}
	}

	public static void createSplitTemporaryVariableMarker(IResource resource, int startIndex, int endIndex, String varName) {
		try {
			IMarker marker = resource.createMarker("reafactoring.splittemporaryvariablemarker");
			marker.setAttribute(IMarker.SEVERITY, IMarker.SEVERITY_WARNING);
			marker.setAttribute(IMarker.MESSAGE,
					"Consider spltting the temporary '" + varName + "' into multiple variables");
			marker.setAttribute(IMarker.CHAR_START, startIndex);
			marker.setAttribute(IMarker.CHAR_END, endIndex);

		} catch (CoreException e) {
			e.printStackTrace();
		}
	}

	public static void createConsolidateMarker(IResource resource, String direction, int startIndex, int endIndex) {
		try {
			IMarker marker = resource.createMarker("reafactoring.consolidateconditionalmarker");
			marker.setAttribute(IMarker.SEVERITY, IMarker.SEVERITY_WARNING);
			marker.setAttribute(IMarker.MESSAGE, "Duplicate conditional fragment: This statement can be " + direction);
			marker.setAttribute(IMarker.CHAR_START, startIndex);
			marker.setAttribute(IMarker.CHAR_END, endIndex);

		} catch (CoreException e) {
			e.printStackTrace();
		}
	}
}
