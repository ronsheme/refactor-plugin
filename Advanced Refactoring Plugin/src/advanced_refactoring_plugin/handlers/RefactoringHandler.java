package advanced_refactoring_plugin.handlers;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Paths;
import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.ui.ide.ResourceUtil;
import org.eclipse.ui.part.FileEditorInput;
import org.eclipse.ui.texteditor.ITextEditor;

import advanced_refactoring_plugin.CommandExecutors;
import advanced_refactoring_plugin.MarkerUtils;
import utils.CompilationPaths;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.text.IDocument;

/**
 * Our sample handler extends AbstractHandler, an IHandler base class.
 * 
 * @see org.eclipse.core.commands.IHandler
 * @see org.eclipse.core.commands.AbstractHandler
 */
public class RefactoringHandler extends AbstractHandler {

	/**
	 * @return content of file that is currently open in the editor
	 */
	private String getCurrentEditorContent() {
		final IEditorPart activeEditor = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage()
				.getActiveEditor();
		if (!(activeEditor instanceof ITextEditor))
			return null;
		ITextEditor ite = (ITextEditor) activeEditor;
		IDocument doc = ite.getDocumentProvider().getDocument(ite.getEditorInput());
		return doc.get();
	}
	
	private FileEditorInput getFileEditorInput(){
		IWorkbench workbench = PlatformUI.getWorkbench();
		IWorkbenchWindow window = workbench == null ? null : workbench.getActiveWorkbenchWindow();
		IWorkbenchPage activePage = window == null ? null : window.getActivePage();

		IEditorPart editor = activePage == null ? null : activePage.getActiveEditor();
		IEditorInput input = editor == null ? null : editor.getEditorInput();
		return input instanceof FileEditorInput? (FileEditorInput) input:null;
	}
	
	/**
	 * @return path of file that is currently open in the editor
	 */
	private String getCurrentEditorContentPath() {
		FileEditorInput fei = getFileEditorInput();
		return fei != null? (new File(fei.getURI())).getAbsolutePath() : null;
	}

	/*
	 * @return content name of file that is currently open in the editor only
	 *         difference from getCurrentEditorContentPath is the usage of Paths
	 *         class to get the path instead of file
	 */
	private String getCurrentEditorContentName() {

		IEditorInput input = getCurrentEditorInput();
		return input instanceof FileEditorInput
				? Paths.get(((FileEditorInput) input).getURI()).toFile().getAbsolutePath() : null;
	}
	
	private IJavaProject getJavaProject()
	{
		IProject project = getFileEditorInput().getFile().getProject();
		try {
			if (project.hasNature(JavaCore.NATURE_ID)) {
			    return JavaCore.create(project);
			}
		} catch (CoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	private IEditorInput getCurrentEditorInput() {
		IWorkbench workbench = PlatformUI.getWorkbench();
		IWorkbenchWindow window = workbench == null ? null : workbench.getActiveWorkbenchWindow();
		IWorkbenchPage activePage = window == null ? null : window.getActivePage();

		IEditorPart editor = activePage == null ? null : activePage.getActiveEditor();
		return editor == null ? null : editor.getEditorInput();
	}

	/**
	 * the command has been executed, so extract the needed information from the
	 * application context. Choose the suitable refactoring according to the
	 * command's context
	 */
	public Object execute(ExecutionEvent event) throws ExecutionException {
		IWorkbenchWindow window = HandlerUtil.getActiveWorkbenchWindowChecked(event);
		String program = getCurrentEditorContent();
		if (program == null)
			return null;
		String programPath = getCurrentEditorContentPath();
		String programName = getCurrentEditorContentName();
		IResource resourceOfCurrentWindow = ResourceUtil.getResource(getCurrentEditorInput());
		IJavaProject project = getJavaProject();
		if(project == null)
			return null;
		CompilationPaths compPaths = new CompilationPaths(programName, program, programPath,project);
		switch (event.getCommand().getId()) {
		case "Advanced_Refactoring_Plugin.commands.replaceTempWithQuery":
			MarkerUtils.deleteMarkers(resourceOfCurrentWindow, "reafactoring.replacetempwitheuqrymarker");
			if(CommandExecutors.runReplaceTempWithQuery(compPaths, resourceOfCurrentWindow))
				MessageDialog.openInformation(window.getShell(), "Refactoring Assistance Plugin",
						"Nothing found");
			break;
		case "Advanced_Refactoring_Plugin.commands.splitTemporaryVariable":
			MarkerUtils.deleteMarkers(resourceOfCurrentWindow, "reafactoring.splittemporaryvariablemarker");
			if(CommandExecutors.runSplitTemporaryVariable(compPaths, resourceOfCurrentWindow))
				MessageDialog.openInformation(window.getShell(), "Refactoring Assistance Plugin",
						"Nothing found");
			break;
		case "Advanced_Refactoring_Plugin.commands.consolidateConditional":
			MarkerUtils.deleteMarkers(resourceOfCurrentWindow, "reafactoring.consolidateconditionalmarker");
			if(CommandExecutors.runConsolidateConditional(compPaths, resourceOfCurrentWindow))
				MessageDialog.openInformation(window.getShell(), "Refactoring Assistance Plugin",
						"Nothing found");
			break;
		case "Advanced_Refactoring_Plugin.commands.removeMarkers":
			MarkerUtils.deleteMarkers(resourceOfCurrentWindow, null);
			break;
		case "Advanced_Refactoring_Plugin.commands.pushDownField":
			MarkerUtils.deleteMarkers(resourceOfCurrentWindow, "reafactoring.pushdownfieldmarker");
			try {
				MessageDialog.openInformation(window.getShell(), "Refactoring Assistance Plugin",
				CommandExecutors.runPushDownField(compPaths, resourceOfCurrentWindow));
			} catch (ClassNotFoundException | IOException e) {
				MessageDialog.openInformation(window.getShell(), "Refactoring Assistance Plugin", "Error");
				e.printStackTrace();
			}
			break;
		case "Advanced_Refactoring_Plugin.commands.introduceParameterObject":
			try {
				MessageDialog.openInformation(window.getShell(), "Refactoring Assistance Plugin",
						CommandExecutors.runIntroduceParameterObject(compPaths));
			} catch (ClassNotFoundException | IOException e) {
				MessageDialog.openInformation(window.getShell(), "Refactoring Assistance Plugin", "Error");
				e.printStackTrace();
			}
			break;
		}
		return null;
	}

	/**
	 * disable all the plugin commands when no Editor is opened
	 */
	@Override
	public boolean isEnabled() {// TODO enable only after successful built
		return getCurrentEditorContent() != null;
	}


}
