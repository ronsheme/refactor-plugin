package advanced_refactoring_plugin;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.commons.io.FilenameUtils;
import org.eclipse.core.resources.IResource;
import org.eclipse.jdt.core.IJavaProject;
import utils.CompilationPaths;
import utils.CompilationUtils;
import utils.IndexPair;
import utils.IntroduceParameterObject;
import utils.VarIndex;
import utils.visitors.ConsolidateConditionalVisitor;
import utils.visitors.PushDownFieldVisitor;
import utils.visitors.ReplaceTempWithQueryVisitor;
import utils.visitors.SplitTemporaryVariableVisitor;

public class CommandExecutors {
	/**
	 * runs a ReplaceTempWithQuery visitor on the given program
	 * 
	 * @param programName
	 * @param program
	 * @param programPath
	 * @param resourceOfCurrentWindow
	 * @return a string describing all the statements in which the assignment to
	 *         a temporary is suggested to be replaced with a query
	 */
	public static boolean runReplaceTempWithQuery(CompilationPaths compPaths, IResource resource) {
		ReplaceTempWithQueryVisitor RTWQVisitor = new ReplaceTempWithQueryVisitor();
		CompilationUtils.runRefactor(compPaths, RTWQVisitor);
		if (RTWQVisitor.getTempsToRefactor().size() == 0)
			return true;
		for (VarIndex ip : RTWQVisitor.getTempPositionsToRefactor()) {
			MarkerUtils.createReplaceTempWithQueryMarker(resource, ip.getIndex(), ip.getIndexEnd());
		}
		return false;
	}

	/**
	 * runs a SplitTemporaryVariable visitor on the given program
	 * 
	 * @param programName
	 * @param program
	 * @param programPath
	 * @param resourceOfCurrentWindow
	 * @return a string describing all the statements in which the assignment to
	 *         a temporary is suggested to be splitter into multiple temporaries
	 */
	public static boolean runSplitTemporaryVariable(CompilationPaths compPaths, IResource resource) {
		SplitTemporaryVariableVisitor STVVisitor = new SplitTemporaryVariableVisitor();
		CompilationUtils.runRefactor(compPaths, STVVisitor);
		if (STVVisitor.getTempsToRefactor().size() == 0)
			return true;
		for (VarIndex ip : STVVisitor.getTempPositionsToRefactor()) {
			MarkerUtils.createSplitTemporaryVariableMarker(resource, ip.getIndex(), ip.getIndexEnd(), ip.getVarName());
		} // markers only first assignment. in order to marker all the
			// assignments it is necessary to save them in
			// SplitTemporaryVariableVisitor
		return false;
	}

	/**
	 * runs a ConsolidateConditional visitor on the given program
	 * 
	 * @param programName
	 * @param program
	 * @param programPath
	 * @return a string describing all the statements in conditional statements
	 *         that can be pulled up or pushed down out the 'Then Block' and
	 *         'Else Block'
	 */
	public static boolean runConsolidateConditional(CompilationPaths compPaths, IResource resource) {
		ConsolidateConditionalVisitor CCVisitor = new ConsolidateConditionalVisitor();
		CompilationUtils.runRefactor(compPaths, CCVisitor);
		List<IndexPair> pullUpList = CCVisitor.getPullUpStatementsLines();
		List<IndexPair> pushDownList = CCVisitor.getPushDownStatementsLines();
		if (pullUpList.size() == 0 && pushDownList.size() == 0)
			return true;

		if (pullUpList.size() != 0) {
			for (Iterator iterator = pullUpList.iterator(); iterator.hasNext();) {
				IndexPair indexPair = (IndexPair) iterator.next();
				MarkerUtils.createConsolidateMarker(resource, "pulled up", indexPair.getIndex1start(),
						indexPair.getIndex1end());
				MarkerUtils.createConsolidateMarker(resource, "pulled up", indexPair.getIndex2start(),
						indexPair.getIndex2end());
			}
		}
		if (pushDownList.size() != 0) {
			for (Iterator iterator = pushDownList.iterator(); iterator.hasNext();) {
				IndexPair indexPair = (IndexPair) iterator.next();
				MarkerUtils.createConsolidateMarker(resource, "pushed down", indexPair.getIndex1start(),
						indexPair.getIndex1end());
				MarkerUtils.createConsolidateMarker(resource, "pushed down up", indexPair.getIndex2start(),
						indexPair.getIndex2end());
			}
		}
		return false;
	}

	public static String runIntroduceParameterObject(CompilationPaths compPaths)
			throws ClassNotFoundException, IOException {

		IntroduceParameterObject ipo = new IntroduceParameterObject(getClassFromClassLoader(compPaths));

		Method[] methods = getClassFromClassLoader(compPaths).getDeclaredMethods();
		if (methods.length < 2) {
			return "Introduce Parameter Object analysis is"
					+ " irrelevant for classes with less than two methods.";
		}
		int max = ipo.maxNumOfMethodParameters();
		if (max < 2) {
			return "Introduce Parameter Object analysis is irrelevant for"
					+ " classes in which all signatures contain 0 or 1 parameters.";
		}
		
		Map<String[], Set<Object>> map = ipo.runClassAnalysis();
		if(map.isEmpty()){
			return "Class analysis has terminated without"
					+ " any clustering recommendations.";
		}
		String l = "The following list of parameter types may be clustered as a new Java object:\n\n";
		for (String[] s : map.keySet()) {
			String paramList = "[";
			for (String string : s) {
				paramList += string + ", ";
			}
			paramList = paramList.substring(0, paramList.length() - 2) + "]";
			for (Object o : map.get(s)) {
				String methodName = "";
				if (o instanceof Method)
					methodName = ((Method) o).getName();
				if (o instanceof Constructor<?>)
					methodName = ((Constructor<?>) o).getName();
				l += paramList + " in method " + methodName + "\n";
			}
			l += "\n";
		}
		return l;
	}

	private static Class<?> getClassFromClassLoader(CompilationPaths compPaths) throws ClassNotFoundException {
		ClassLoader currThreadClassLoader = Thread.currentThread().getContextClassLoader();
		String className = FilenameUtils.getBaseName(compPaths.sourcePath);
		String packageName = CompilationUtils.getPackageNameFromFilePath(compPaths);
		return currThreadClassLoader.loadClass(packageName + "." + className);
	}

	public static String runPushDownField(CompilationPaths compPaths, IResource resource)
			throws ClassNotFoundException, FileNotFoundException {
		Class<?> c = getClassFromClassLoader(compPaths);
		PushDownFieldVisitor p = new PushDownFieldVisitor(c,compPaths);
		p.resetAnalysisResult();
		String analysis = p.runClassAnalysis();
			return analysis;
	}
}
